// Copyright IBM Corp. 2019,2020. All Rights Reserved.
// Node module: loopback4-example-shopping
// This file is licensed under the MIT License.
// License text available at https://opensource.org/licenses/MIT

import {Getter, inject} from '@loopback/core';
import {
  AnyObject,
  DataObject,
  DeepPartial,
  DefaultCrudRepository,
  HasOneRepositoryFactory,
  repository,
  HasManyRepositoryFactory,
} from '@loopback/repository';
import {User, UserCredentials, Notification, Staff, Teacher} from '../models';
import {UserCredentialsRepository} from './user-credentials.repository';
import moment from 'moment';
import {PostgresDataSource} from '../datasources';
import {NotificationRepository} from './notification.repository';
import {StaffRepository} from './staff.repository';
import {changeToSlug} from '../utils';
import {TeacherRepository} from './teacher.repository';

export type Credentials = {
  email: string;
  password: string;
};

export class UserRepository extends DefaultCrudRepository<
  User,
  typeof User.prototype.id
> {
  public readonly userCredentials: HasOneRepositoryFactory<
    UserCredentials,
    typeof User.prototype.id
  >;

  public readonly notifications: HasManyRepositoryFactory<
    Notification,
    typeof User.prototype.id
  >;

  public readonly staff: HasOneRepositoryFactory<
    Staff,
    typeof User.prototype.id
  >;

  public readonly teacher: HasOneRepositoryFactory<
    Teacher,
    typeof User.prototype.id
  >;

  constructor(
    @inject('datasources.postgres') dataSource: PostgresDataSource,
    @repository.getter('UserCredentialsRepository')
    protected userCredentialsRepositoryGetter: Getter<UserCredentialsRepository>,
    @repository.getter('NotificationRepository')
    protected notificationRepositoryGetter: Getter<NotificationRepository>,
    @repository.getter('StaffRepository')
    protected staffRepositoryGetter: Getter<StaffRepository>,
    @repository.getter('TeacherRepository')
    protected teacherRepositoryGetter: Getter<TeacherRepository>,
  ) {
    super(User, dataSource);
    this.teacher = this.createHasOneRepositoryFactoryFor(
      'teacher',
      teacherRepositoryGetter,
    );
    this.registerInclusionResolver('teacher', this.teacher.inclusionResolver);
    this.staff = this.createHasOneRepositoryFactoryFor(
      'staff',
      staffRepositoryGetter,
    );
    this.notifications = this.createHasManyRepositoryFactoryFor(
      'notifications',
      notificationRepositoryGetter,
    );
    this.registerInclusionResolver(
      'notifications',
      this.notifications.inclusionResolver,
    );
    this.userCredentials = this.createHasOneRepositoryFactoryFor(
      'userCredentials',
      userCredentialsRepositoryGetter,
    );
  }

  async findCredentials(
    userId: typeof User.prototype.id,
  ): Promise<UserCredentials | undefined> {
    try {
      return await this.userCredentials(userId).get();
    } catch (err) {
      if (err.code === 'ENTITY_NOT_FOUND') {
        return undefined;
      }
      throw err;
    }
  }

  async create(
    entity: Partial<User> | {[P in keyof User]?: DeepPartial<User[P]>} | User,
    options?: AnyObject,
  ): Promise<User> {
    const newData = {
      ...entity,
      createdAt: moment().utc().toISOString(),
      ...(entity?.firstName && entity?.lastName
        ? {
            slug: `${changeToSlug(entity?.firstName)}-${changeToSlug(
              entity?.lastName,
            )}`,
          }
        : ''),
    };

    return super.create(newData, options);
  }

  async updateById(
    id: number,
    data: DataObject<User>,
    options?: AnyObject,
  ): Promise<any> {
    const newData = {
      ...data,
      updatedAt: moment().utc().toISOString(),
    };

    super.updateById(id, newData, options);

    return super.findById(id);
  }
}
