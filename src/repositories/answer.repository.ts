import {Getter, inject} from '@loopback/core';
import {
  BelongsToAccessor,
  DataObject,
  DeepPartial,
  DefaultTransactionalRepository,
  Options,
  repository,
} from '@loopback/repository';
import moment from 'moment';
import {PostgresDataSource} from '../datasources';
import {Question, Answer, AnswerRelations, User} from '../models';
import {QuestionRepository} from './question.repository';
import {UserRepository} from './user.repository';

export class AnswerRepository extends DefaultTransactionalRepository<
  Answer,
  typeof Answer.prototype.id,
  AnswerRelations
> {
  public readonly creator: BelongsToAccessor<User, typeof Answer.prototype.id>;

  public readonly question: BelongsToAccessor<
    Question,
    typeof Answer.prototype.id
  >;

  constructor(
    @inject('datasources.postgres') dataSource: PostgresDataSource,
    @repository.getter('UserRepository')
    protected userRepositoryGetter: Getter<UserRepository>,
    @repository.getter('QuestionRepository')
    protected questionRepositoryGetter: Getter<QuestionRepository>,
  ) {
    super(Answer, dataSource);
    this.creator = this.createBelongsToAccessorFor(
      'creator',
      userRepositoryGetter,
    );
    this.registerInclusionResolver('creator', this.creator.inclusionResolver);
    this.question = this.createBelongsToAccessorFor(
      'question',
      questionRepositoryGetter,
    );
    this.registerInclusionResolver('question', this.question.inclusionResolver);
  }

  async create(
    entity:
      | Partial<Answer>
      | {[P in keyof Answer]?: DeepPartial<Answer[P]>}
      | Answer,
    options?: Options,
  ): Promise<Answer> {
    const newData = {
      ...entity,
      createdAt: moment().utc().toISOString(),
      updatedAt: moment().utc().toISOString(),
    };

    return super.create(newData, options);
  }

  async updateById(
    id: number,
    data: DataObject<Answer>,
    options?: Options,
  ): Promise<void> {
    try {
      const payload = {
        ...data,
        updatedAt: moment().utc().toISOString(),
      };
      await super.updateById(id, payload, options);
    } catch (err) {
      throw err;
    }
  }
}
