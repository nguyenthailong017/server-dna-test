import {inject} from '@loopback/core';
import {
  AnyObject,
  DataObject,
  DeepPartial,
  DefaultCrudRepository,
} from '@loopback/repository';
import moment from 'moment';
import {PostgresDataSource} from '../datasources';
import {
  ExamManagement,
  ExamManagementRelations,
  EXAM_STATUS_TODO,
} from '../models';

export class ExamManagementRepository extends DefaultCrudRepository<
  ExamManagement,
  typeof ExamManagement.prototype.id,
  ExamManagementRelations
> {
  constructor(@inject('datasources.postgres') dataSource: PostgresDataSource) {
    super(ExamManagement, dataSource);
  }

  async create(
    entity:
      | Partial<ExamManagement>
      | {[P in keyof ExamManagement]?: DeepPartial<ExamManagement[P]>}
      | ExamManagement,
    options?: AnyObject,
  ): Promise<ExamManagement> {
    const newData = {
      ...entity,
      createdAt: moment().utc().toISOString(),
      status: EXAM_STATUS_TODO,
    };

    return super.create(newData, options);
  }

  async updateById(
    id: number,
    data: DataObject<ExamManagement>,
    options?: AnyObject,
  ): Promise<void> {
    try {
      const payload = {
        ...data,
        updatedAt: moment().utc().toISOString(),
      };

      await super.updateById(id, payload, options);
    } catch (error) {
      throw error;
    }
  }
}
