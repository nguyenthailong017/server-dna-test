import {inject, Getter} from '@loopback/core';
import {
  DefaultCrudRepository,
  repository,
  BelongsToAccessor,
  DeepPartial,
  AnyObject,
  DataObject,
} from '@loopback/repository';
import moment from 'moment';
import {PostgresDataSource} from '../datasources';
import {StaffSchedule, StaffScheduleRelations, User} from '../models';
import {UserRepository} from './user.repository';

export class StaffScheduleRepository extends DefaultCrudRepository<
  StaffSchedule,
  typeof StaffSchedule.prototype.id,
  StaffScheduleRelations
> {
  public readonly user: BelongsToAccessor<
    User,
    typeof StaffSchedule.prototype.id
  >;

  constructor(
    @inject('datasources.postgres') dataSource: PostgresDataSource,
    @repository.getter('UserRepository')
    protected userRepositoryGetter: Getter<UserRepository>,
  ) {
    super(StaffSchedule, dataSource);
    this.user = this.createBelongsToAccessorFor('user', userRepositoryGetter);
    this.registerInclusionResolver('user', this.user.inclusionResolver);
  }

  async create(
    entity:
      | Partial<StaffSchedule>
      | {[P in keyof StaffSchedule]?: DeepPartial<StaffSchedule[P]>}
      | StaffSchedule,
    options?: AnyObject,
  ): Promise<StaffSchedule> {
    const newData = {
      ...entity,
      createdAt: moment().utc().toISOString(),
    };

    return super.create(newData, options);
  }

  async updateById(
    id: number,
    data: DataObject<StaffSchedule>,
    options?: AnyObject,
  ): Promise<void> {
    try {
      const payload = {
        ...data,
        updatedAt: moment().utc().toISOString(),
      };

      await super.updateById(id, payload, options);
    } catch (error) {
      throw error;
    }
  }
}
