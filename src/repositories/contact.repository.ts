import {inject} from '@loopback/core';
import {
  AnyObject,
  DataObject,
  DeepPartial,
  DefaultCrudRepository,
} from '@loopback/repository';
import moment from 'moment';
import {PostgresDataSource} from '../datasources';
import {Contact, ContactRelations, TODO} from '../models';

export class ContactRepository extends DefaultCrudRepository<
  Contact,
  typeof Contact.prototype.id,
  ContactRelations
> {
  constructor(@inject('datasources.postgres') dataSource: PostgresDataSource) {
    super(Contact, dataSource);
  }

  async create(
    entity:
      | Partial<Contact>
      | {[P in keyof Contact]?: DeepPartial<Contact[P]>}
      | Contact,
    options?: AnyObject,
  ): Promise<Contact> {
    const newData = {
      ...entity,
      createdAt: moment().utc().toISOString(),
      status: TODO,
    };

    return super.create(newData, options);
  }

  async updateById(
    id: number,
    data: DataObject<Contact>,
    options?: AnyObject,
  ): Promise<void> {
    try {
      const payload = {
        ...data,
        updatedAt: moment().utc().toISOString(),
      };

      await super.updateById(id, payload, options);
    } catch (error) {
      throw error;
    }
  }
}
