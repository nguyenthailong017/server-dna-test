import {inject, Getter} from '@loopback/core';
import {DefaultCrudRepository, repository, BelongsToAccessor} from '@loopback/repository';
import {PostgresDataSource} from '../datasources';
import {StudentReview, StudentReviewRelations, User, Classroom} from '../models';
import {UserRepository} from './user.repository';
import {ClassroomRepository} from './classroom.repository';

export class StudentReviewRepository extends DefaultCrudRepository<
  StudentReview,
  typeof StudentReview.prototype.id,
  StudentReviewRelations
> {

  public readonly user: BelongsToAccessor<User, typeof StudentReview.prototype.id>;

  public readonly classroom: BelongsToAccessor<Classroom, typeof StudentReview.prototype.id>;

  constructor(@inject('datasources.postgres') dataSource: PostgresDataSource, @repository.getter('UserRepository') protected userRepositoryGetter: Getter<UserRepository>, @repository.getter('ClassroomRepository') protected classroomRepositoryGetter: Getter<ClassroomRepository>,) {
    super(StudentReview, dataSource);
    this.classroom = this.createBelongsToAccessorFor('classroom', classroomRepositoryGetter,);
    this.registerInclusionResolver('classroom', this.classroom.inclusionResolver);
    this.user = this.createBelongsToAccessorFor('user', userRepositoryGetter,);
    this.registerInclusionResolver('user', this.user.inclusionResolver);
  }
}
