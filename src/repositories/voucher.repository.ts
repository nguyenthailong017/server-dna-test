import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {PostgresDataSource} from '../datasources';
import {Voucher, VoucherRelations} from '../models';

export class VoucherRepository extends DefaultCrudRepository<
  Voucher,
  typeof Voucher.prototype.id,
  VoucherRelations
> {
  constructor(@inject('datasources.postgres') dataSource: PostgresDataSource) {
    super(Voucher, dataSource);
  }
}
