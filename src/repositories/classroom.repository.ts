import {inject, Getter} from '@loopback/core';
import {
  DefaultTransactionalRepository,
  repository,
  BelongsToAccessor,
  AnyObject,
  DeepPartial,
  DataObject,
  Options,
} from '@loopback/repository';
import {PostgresDataSource} from '../datasources';
import {Classroom, ClassroomRelations, Courses} from '../models';
import {CoursesRepository} from './courses.repository';
import moment from 'moment';

export class ClassroomRepository extends DefaultTransactionalRepository<
  Classroom,
  typeof Classroom.prototype.id,
  ClassroomRelations
> {
  public readonly courses: BelongsToAccessor<
    Courses,
    typeof Classroom.prototype.id
  >;

  constructor(
    @inject('datasources.postgres') dataSource: PostgresDataSource,
    @repository.getter('CoursesRepository')
    protected coursesRepositoryGetter: Getter<CoursesRepository>,
  ) {
    super(Classroom, dataSource);
    this.courses = this.createBelongsToAccessorFor(
      'courses',
      coursesRepositoryGetter,
    );
    this.registerInclusionResolver('courses', this.courses.inclusionResolver);
  }

  async create(
    entity:
      | Partial<Classroom>
      | {[P in keyof Classroom]?: DeepPartial<Classroom[P]>}
      | Classroom,
    options?: AnyObject,
  ): Promise<Classroom> {
    const newData = {
      ...entity,
      createdAt: moment().utc().toISOString(),
      updatedAt: moment().utc().toISOString(),
    };

    return super.create(newData, options);
  }

  async updateById(
    id: number,
    data: DataObject<Classroom>,
    options?: Options,
  ): Promise<void> {
    try {
      const payload = {
        ...data,
        updatedAt: moment().utc().toISOString(),
      };
      await super.updateById(id, payload, options);
    } catch (err) {
      throw err;
    }
  }
}
