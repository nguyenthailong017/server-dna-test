import {inject} from '@loopback/core';
import {
  AnyObject,
  DataObject,
  DeepPartial,
  DefaultCrudRepository,
} from '@loopback/repository';
import moment from 'moment';
import {PostgresDataSource} from '../datasources';
import {
  ChangeSchedule,
  ChangeScheduleRelations,
  CS_STATUS_TODO,
} from '../models';

export class ChangeScheduleRepository extends DefaultCrudRepository<
  ChangeSchedule,
  typeof ChangeSchedule.prototype.id,
  ChangeScheduleRelations
> {
  constructor(@inject('datasources.postgres') dataSource: PostgresDataSource) {
    super(ChangeSchedule, dataSource);
  }

  async create(
    entity:
      | Partial<ChangeSchedule>
      | {[P in keyof ChangeSchedule]?: DeepPartial<ChangeSchedule[P]>}
      | ChangeSchedule,
    options?: AnyObject,
  ): Promise<ChangeSchedule> {
    const newData = {
      ...entity,
      createdAt: moment().utc().toISOString(),
      status: CS_STATUS_TODO,
    };

    return super.create(newData, options);
  }

  async updateById(
    id: number,
    data: DataObject<ChangeSchedule>,
    options?: AnyObject,
  ): Promise<void> {
    try {
      const payload = {
        ...data,
        updatedAt: moment().utc().toISOString(),
      };

      await super.updateById(id, payload, options);
    } catch (error) {
      throw error;
    }
  }
}
