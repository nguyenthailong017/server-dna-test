import {Getter, inject} from '@loopback/core';
import {
  DataObject,
  DeepPartial,
  DefaultCrudRepository,
  HasOneRepositoryFactory,
  Options,
  repository,
} from '@loopback/repository';
import moment from 'moment';
import {PostgresDataSource} from '../datasources';
import {
  HightlightTeacher,
  HightlightTeacherRelations,
  MediaContent,
} from '../models';
import {MediaContentRepository} from './media-content.repository';

export class HightlightTeacherRepository extends DefaultCrudRepository<
  HightlightTeacher,
  typeof HightlightTeacher.prototype.id,
  HightlightTeacherRelations
> {

  public readonly mediaContent: HasOneRepositoryFactory<MediaContent, typeof HightlightTeacher.prototype.id>;

  constructor(
    @inject('datasources.postgres') dataSource: PostgresDataSource,
    @repository.getter('MediaContentRepository')
    protected mediaContentRepositoryGetter: Getter<MediaContentRepository>,
  ) {
    super(HightlightTeacher, dataSource);
    this.mediaContent = this.createHasOneRepositoryFactoryFor('mediaContent', mediaContentRepositoryGetter);
    this.registerInclusionResolver('mediaContent', this.mediaContent.inclusionResolver);

  }

  async create(
    entity:
      | Partial<HightlightTeacher>
      | {[P in keyof HightlightTeacher]?: DeepPartial<HightlightTeacher[P]>}
      | HightlightTeacher,
    options?: Options,
  ): Promise<HightlightTeacher> {
    const newData = {
      ...entity,
      createdAt: moment().utc().toISOString(),
      updatedAt: moment().utc().toISOString(),
    };

    return super.create(newData, options);
  }

  async updateById(
    id: number,
    data: DataObject<HightlightTeacher>,
    options?: Options,
  ): Promise<void> {
    try {
      const payload = {
        ...data,
        updatedAt: moment().utc().toISOString(),
      };
      await super.updateById(id, payload, options);
    } catch (err) {
      throw err;
    }
  }
}
