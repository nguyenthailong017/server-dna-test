import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {PostgresDataSource} from '../datasources';
import {Shift, ShiftRelations} from '../models';

export class ShiftRepository extends DefaultCrudRepository<
  Shift,
  typeof Shift.prototype.id,
  ShiftRelations
> {
  constructor(@inject('datasources.postgres') dataSource: PostgresDataSource) {
    super(Shift, dataSource);
  }
}
