import {inject, Getter} from '@loopback/core';
import {
  DefaultCrudRepository,
  repository,
  HasManyRepositoryFactory,
  AnyObject,
  DeepPartial,
  HasOneRepositoryFactory,
} from '@loopback/repository';
import moment from 'moment';
import {PostgresDataSource} from '../datasources';
import {Review, ReviewRelations, MediaContent} from '../models';
import {MediaContentRepository} from './media-content.repository';

export class ReviewRepository extends DefaultCrudRepository<
  Review,
  typeof Review.prototype.id,
  ReviewRelations
> {
  public readonly mediaContent: HasOneRepositoryFactory<
    MediaContent,
    typeof Review.prototype.id
  >;

  constructor(
    @inject('datasources.postgres') dataSource: PostgresDataSource,
    @repository.getter('MediaContentRepository')
    protected mediaContentRepositoryGetter: Getter<MediaContentRepository>,
  ) {
    super(Review, dataSource);
    this.mediaContent = this.createHasOneRepositoryFactoryFor(
      'mediaContent',
      mediaContentRepositoryGetter,
    );
    this.registerInclusionResolver(
      'mediaContent',
      this.mediaContent.inclusionResolver,
    );
  }

  async create(
    entity:
      | Partial<Review>
      | {[P in keyof Review]?: DeepPartial<Review[P]>}
      | Review,
    options?: AnyObject,
  ): Promise<Review> {
    const newData = {
      ...entity,
      createdAt: moment().utc().toISOString(),
    };

    return super.create(newData, options);
  }
}
