import {inject, Getter} from '@loopback/core';
import {
  DefaultTransactionalRepository,
  repository,
  HasOneRepositoryFactory,
  BelongsToAccessor,
} from '@loopback/repository';
import {PostgresDataSource} from '../datasources';
import {
  Schedule,
  ScheduleRelations,
  ChangeSchedule,
  Classroom,
  Shift,
  Room,
} from '../models';
import {ChangeScheduleRepository} from './change-schedule.repository';
import {ClassroomRepository} from './classroom.repository';
import {ShiftRepository} from './shift.repository';
import {RoomRepository} from './room.repository';

export class ScheduleRepository extends DefaultTransactionalRepository<
  Schedule,
  typeof Schedule.prototype.id,
  ScheduleRelations
> {
  public readonly changeSchedule: HasOneRepositoryFactory<
    ChangeSchedule,
    typeof Schedule.prototype.id
  >;

  public readonly classroom: BelongsToAccessor<
    Classroom,
    typeof Schedule.prototype.id
  >;

  public readonly shift: BelongsToAccessor<Shift, typeof Schedule.prototype.id>;

  public readonly room: BelongsToAccessor<Room, typeof Schedule.prototype.id>;

  constructor(
    @inject('datasources.postgres') dataSource: PostgresDataSource,
    @repository.getter('ChangeScheduleRepository')
    protected changeScheduleRepositoryGetter: Getter<ChangeScheduleRepository>,
    @repository.getter('ClassroomRepository')
    protected classroomRepositoryGetter: Getter<ClassroomRepository>,
    @repository.getter('ShiftRepository')
    protected shiftRepositoryGetter: Getter<ShiftRepository>,
    @repository.getter('RoomRepository')
    protected roomRepositoryGetter: Getter<RoomRepository>,
  ) {
    super(Schedule, dataSource);
    this.room = this.createBelongsToAccessorFor('room', roomRepositoryGetter);
    this.registerInclusionResolver('room', this.room.inclusionResolver);
    this.shift = this.createBelongsToAccessorFor(
      'shift',
      shiftRepositoryGetter,
    );
    this.registerInclusionResolver('shift', this.shift.inclusionResolver);
    this.classroom = this.createBelongsToAccessorFor(
      'classroom',
      classroomRepositoryGetter,
    );
    this.registerInclusionResolver(
      'classroom',
      this.classroom.inclusionResolver,
    );
    this.changeSchedule = this.createHasOneRepositoryFactoryFor(
      'changeSchedule',
      changeScheduleRepositoryGetter,
    );
    this.registerInclusionResolver(
      'changeSchedule',
      this.changeSchedule.inclusionResolver,
    );
  }
}
