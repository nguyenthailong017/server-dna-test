import {Getter, inject} from '@loopback/core';
import {
  AnyObject,
  BelongsToAccessor,
  DeepPartial,
  DefaultCrudRepository,
  repository,
} from '@loopback/repository';
import moment from 'moment';
import {PostgresDataSource} from '../datasources';
import {MediaContent, MediaContentRelations, News, Review} from '../models';
import {NewsRepository, ReviewRepository} from '.';

export class MediaContentRepository extends DefaultCrudRepository<
  MediaContent,
  typeof MediaContent.prototype.id,
  MediaContentRelations
> {
  public readonly news: BelongsToAccessor<
    News,
    typeof MediaContent.prototype.id
  >;
  public readonly review: BelongsToAccessor<
    Review,
    typeof MediaContent.prototype.id
  >;

  constructor(
    @inject('datasources.postgres') dataSource: PostgresDataSource,
    @repository.getter('NewsRepository')
    protected newsRepositoryGetter: Getter<NewsRepository>,
    @repository.getter('ReviewRepository')
    protected reviewRepositoryGetter: Getter<ReviewRepository>,
  ) {
    super(MediaContent, dataSource);
    this.news = this.createBelongsToAccessorFor('news', newsRepositoryGetter);
    this.registerInclusionResolver('news', this.news.inclusionResolver);
    this.review = this.createBelongsToAccessorFor(
      'review',
      reviewRepositoryGetter,
    );
    this.registerInclusionResolver('review', this.news.inclusionResolver);
  }

  async create(
    entity:
      | Partial<MediaContent>
      | {[P in keyof MediaContent]?: DeepPartial<MediaContent[P]>}
      | MediaContent,
    options?: AnyObject,
  ): Promise<MediaContent> {
    const newData = {
      createdAt: moment().utc().toISOString(),
      ...entity,
    };

    return super.create(newData, options);
  }
}
