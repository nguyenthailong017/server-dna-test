import {Getter, inject} from '@loopback/core';
import {
  DataObject,
  DeepPartial,
  DefaultTransactionalRepository,
  HasOneRepositoryFactory,
  Options,
  repository,
} from '@loopback/repository';
import moment from 'moment';
import {PostgresDataSource} from '../datasources';
import {Question, QuestionRelations, Answer} from '../models';
import { AnswerRepository } from './answer.repository';

export class QuestionRepository extends DefaultTransactionalRepository<
  Question,
  typeof Question.prototype.id,
  QuestionRelations
> {
  public readonly answerResult: HasOneRepositoryFactory<
    Answer,
    typeof Question.prototype.id
  >;

  constructor(
    @inject('datasources.postgres') dataSource: PostgresDataSource,
    @repository.getter('AnswerRepository')
    protected answerRepositoryGetter: Getter<AnswerRepository>,
  ) {
    super(Question, dataSource);
    this.answerResult = this.createHasOneRepositoryFactoryFor(
      'answerResult',
      answerRepositoryGetter,
    );
    this.registerInclusionResolver('answerResult', this.answerResult.inclusionResolver);
  }

  async create(
    entity: Partial<Question> | {[P in keyof Question]?: DeepPartial<Question[P]>} | Question,
    options?: Options,
  ): Promise<Question> {
    const newData = {
      ...entity,
      createdAt: moment().utc().toISOString(),
      updatedAt: moment().utc().toISOString(),
    };

    return super.create(newData, options);
  }

  async updateById(
    id: number,
    data: DataObject<Question>,
    options?: Options,
  ): Promise<void> {
    try {
      const payload = {
        ...data,
        updatedAt: moment().utc().toISOString(),
      };
      await super.updateById(id, payload, options);
    } catch (err) {
      throw err;
    }
  }
}
