import {inject, Getter} from '@loopback/core';
import {
  DefaultCrudRepository,
  repository,
  HasManyRepositoryFactory,
  DeepPartial,
  AnyObject,
  BelongsToAccessor,
} from '@loopback/repository';
import {PostgresDataSource} from '../datasources';
import {News, NewsRelations, MediaContent, User} from '../models';
import {MediaContentRepository} from './media-content.repository';
import moment from 'moment';
import {changeToSlug} from '../utils';
import {UserRepository} from './user.repository';
import {DataObject, Options} from '@loopback/repository/src/common-types';

export class NewsRepository extends DefaultCrudRepository<
  News,
  typeof News.prototype.id,
  NewsRelations
> {
  public readonly mediaContents: HasManyRepositoryFactory<
    MediaContent,
    typeof News.prototype.id
  >;

  public readonly creator: BelongsToAccessor<User, typeof News.prototype.id>;

  constructor(
    @inject('datasources.postgres') dataSource: PostgresDataSource,
    @repository.getter('MediaContentRepository')
    protected mediaContentRepositoryGetter: Getter<MediaContentRepository>,
    @repository.getter('UserRepository')
    protected userRepositoryGetter: Getter<UserRepository>,
  ) {
    super(News, dataSource);
    this.creator = this.createBelongsToAccessorFor(
      'creator',
      userRepositoryGetter,
    );
    this.registerInclusionResolver('creator', this.creator.inclusionResolver);
    this.mediaContents = this.createHasManyRepositoryFactoryFor(
      'mediaContents',
      mediaContentRepositoryGetter,
    );
    this.registerInclusionResolver(
      'mediaContents',
      this.mediaContents.inclusionResolver,
    );
  }

  async create(
    entity: Partial<News> | {[P in keyof News]?: DeepPartial<News[P]>} | News,
    options?: AnyObject,
  ): Promise<News> {
    const newData = {
      ...entity,
      createdAt: moment().utc().toISOString(),
      updatedAt: moment().utc().toISOString(),
      slug: changeToSlug(entity.title),
    };

    return super.create(newData, options);
  }

  async updateById(
    id: number,
    data: DataObject<News>,
    options?: Options,
  ): Promise<void> {
    try {
      const payload = {
        ...data,
        updatedAt: moment().utc().toISOString(),
        ... data.title ? {slug: changeToSlug(data.title),}: {}
      };
      await super.updateById(id, payload, options);
    } catch (err) {
      throw err;
    }
  }
}
