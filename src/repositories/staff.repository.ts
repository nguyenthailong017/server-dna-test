import {Getter, inject} from '@loopback/core';
import {
  BelongsToAccessor,
  DataObject,
  DefaultCrudRepository,
  HasManyRepositoryFactory,
  Options,
  repository,
} from '@loopback/repository';
import {PostgresDataSource} from '../datasources';
import {Department, Staff, StaffRelations, Timekeeping, User} from '../models';
import {DepartmentRepository} from './department.repository';
import {TimekeepingRepository} from './timekeeping.repository';
import {UserRepository} from './user.repository';

export class StaffRepository extends DefaultCrudRepository<
  Staff,
  typeof Staff.prototype.id,
  StaffRelations
> {
  public readonly timekeepings: HasManyRepositoryFactory<
    Timekeeping,
    typeof Staff.prototype.id
  >;
  public readonly department: BelongsToAccessor<
    Department,
    typeof Staff.prototype.id
  >;

  public readonly user: BelongsToAccessor<User, typeof Staff.prototype.id>;

  constructor(
    @inject('datasources.postgres') dataSource: PostgresDataSource,
    @repository.getter('TimekeepingRepository')
    protected timekeepingRepositoryGetter: Getter<TimekeepingRepository>,
    @repository.getter('DepartmentRepository')
    protected departmentRepositoryGetter: Getter<DepartmentRepository>,
    @repository.getter('UserRepository')
    protected userRepositoryGetter: Getter<UserRepository>,
  ) {
    super(Staff, dataSource);
    this.user = this.createBelongsToAccessorFor('user', userRepositoryGetter);
    this.registerInclusionResolver('user', this.user.inclusionResolver);
    this.timekeepings = this.createHasManyRepositoryFactoryFor(
      'timekeepings',
      timekeepingRepositoryGetter,
    );
    this.registerInclusionResolver(
      'timekeepings',
      this.timekeepings.inclusionResolver,
    );
    this.department = this.createBelongsToAccessorFor(
      'department',
      departmentRepositoryGetter,
    );
    this.registerInclusionResolver(
      'department',
      this.department.inclusionResolver,
    );
  }

  async updateById(
    id: number,
    data: DataObject<Staff>,
    options?: Options,
  ): Promise<any> {
    try {
      await super.updateById(id, data, options);

      return await super.findById(id);
    } catch (err) {
      throw err;
    }
  }
}
