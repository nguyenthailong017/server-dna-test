import {inject, Getter} from '@loopback/core';
import {DefaultCrudRepository, repository, BelongsToAccessor} from '@loopback/repository';
import {PostgresDataSource} from '../datasources';
import {Attendance, AttendanceRelations, User, Schedule, Classroom} from '../models';
import {UserRepository} from './user.repository';
import {ScheduleRepository} from './schedule.repository';
import {ClassroomRepository} from './classroom.repository';

export class AttendanceRepository extends DefaultCrudRepository<
  Attendance,
  typeof Attendance.prototype.id,
  AttendanceRelations
> {

  public readonly user: BelongsToAccessor<User, typeof Attendance.prototype.id>;

  public readonly schedule: BelongsToAccessor<Schedule, typeof Attendance.prototype.id>;

  public readonly classroom: BelongsToAccessor<Classroom, typeof Attendance.prototype.id>;

  constructor(@inject('datasources.postgres') dataSource: PostgresDataSource, @repository.getter('UserRepository') protected userRepositoryGetter: Getter<UserRepository>, @repository.getter('ScheduleRepository') protected scheduleRepositoryGetter: Getter<ScheduleRepository>, @repository.getter('ClassroomRepository') protected classroomRepositoryGetter: Getter<ClassroomRepository>,) {
    super(Attendance, dataSource);
    this.classroom = this.createBelongsToAccessorFor('classroom', classroomRepositoryGetter,);
    this.registerInclusionResolver('classroom', this.classroom.inclusionResolver);
    this.schedule = this.createBelongsToAccessorFor('schedule', scheduleRepositoryGetter,);
    this.registerInclusionResolver('schedule', this.schedule.inclusionResolver);
    this.user = this.createBelongsToAccessorFor('user', userRepositoryGetter,);
    this.registerInclusionResolver('user', this.user.inclusionResolver);
  }
}
