import {Getter, inject} from '@loopback/core';
import {
  DefaultCrudRepository,
  HasManyRepositoryFactory,
  repository,
  BelongsToAccessor,
  DataObject,
  Options,
} from '@loopback/repository';
import {PostgresDataSource} from '../datasources';
import {
  ChangeSchedule,
  Student,
  StudentRelations,
  User,
  Customer,
  Classroom,
} from '../models';
import {ChangeScheduleRepository} from './change-schedule.repository';
import {UserRepository} from './user.repository';
import {CustomerRepository} from './customer.repository';
import {ClassroomRepository} from './classroom.repository';

export class StudentRepository extends DefaultCrudRepository<
  Student,
  typeof Student.prototype.id,
  StudentRelations
> {
  public readonly changeSchedules: HasManyRepositoryFactory<
    ChangeSchedule,
    typeof Student.prototype.id
  >;

  public readonly classroom: BelongsToAccessor<
    Classroom,
    typeof Student.prototype.id
  >;

  public readonly user: BelongsToAccessor<User, typeof Student.prototype.id>;

  public readonly customer: BelongsToAccessor<
    Customer,
    typeof Student.prototype.id
  >;

  constructor(
    @inject('datasources.postgres') dataSource: PostgresDataSource,
    @repository.getter('ChangeScheduleRepository')
    protected changeScheduleRepositoryGetter: Getter<ChangeScheduleRepository>,
    @repository.getter('UserRepository')
    protected userRepositoryGetter: Getter<UserRepository>,
    @repository.getter('CustomerRepository')
    protected customerRepositoryGetter: Getter<CustomerRepository>,
    @repository.getter('ClassroomRepository')
    protected classroomRepositoryGetter: Getter<ClassroomRepository>,
  ) {
    super(Student, dataSource);
    this.customer = this.createBelongsToAccessorFor(
      'customer',
      customerRepositoryGetter,
    );
    this.registerInclusionResolver('customer', this.customer.inclusionResolver);
    this.user = this.createBelongsToAccessorFor('user', userRepositoryGetter);
    this.registerInclusionResolver('user', this.user.inclusionResolver);

    this.classroom = this.createBelongsToAccessorFor(
      'classroom',
      classroomRepositoryGetter,
    );
    this.registerInclusionResolver(
      'classroom',
      this.classroom.inclusionResolver,
    );

    this.changeSchedules = this.createHasManyRepositoryFactoryFor(
      'changeSchedules',
      changeScheduleRepositoryGetter,
    );
    this.registerInclusionResolver(
      'changeSchedules',
      this.changeSchedules.inclusionResolver,
    );
  }

  async updateById(
    id: number,
    data: DataObject<Student>,
    options?: Options,
  ): Promise<any> {
    try {
      await super.updateById(id, data, options);

      return await super.findById(id);
    } catch (err) {
      throw err;
    }
  }
}
