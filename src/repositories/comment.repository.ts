import {inject} from '@loopback/core';
import {
  AnyObject,
  DataObject,
  DeepPartial,
  DefaultCrudRepository,
  Options,
} from '@loopback/repository';
import moment from 'moment';
import {PostgresDataSource} from '../datasources';
import {Comment, CommentRelations} from '../models';

export class CommentRepository extends DefaultCrudRepository<
  Comment,
  typeof Comment.prototype.id,
  CommentRelations
> {
  constructor(@inject('datasources.postgres') dataSource: PostgresDataSource) {
    super(Comment, dataSource);
  }

  async create(
    entity:
      | Partial<Comment>
      | {[P in keyof Comment]?: DeepPartial<Comment[P]>}
      | Comment,
    options?: AnyObject,
  ): Promise<Comment> {
    const newData = {
      ...entity,
      isParentComment: !entity.commentId,
      createdAt: moment().utc().toISOString(),
    };

    return super.create(newData, options);
  }

  async updateById(
    id: number,
    data: DataObject<Comment>,
    options?: Options,
  ): Promise<void> {
    try {
      const payload = {
        ...data,
        updatedAt: moment().utc().toISOString(),
      };
      await super.updateById(id, payload, options);
    } catch (err) {
      throw err;
    }
  }
}
