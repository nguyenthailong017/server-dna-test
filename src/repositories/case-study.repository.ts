import {Getter, inject} from '@loopback/core';
import {AnyObject, BelongsToAccessor, DataObject, DeepPartial, DefaultCrudRepository, HasOneRepositoryFactory, Options, repository} from '@loopback/repository';
import moment from 'moment';
import {PostgresDataSource} from '../datasources';
import {CaseStudy, CaseStudyRelations, MediaContent, User} from '../models';
import { changeToSlug } from '../utils';
import { MediaContentRepository } from './media-content.repository';
import { UserRepository } from './user.repository';

export class CaseStudyRepository extends DefaultCrudRepository<
  CaseStudy,
  typeof CaseStudy.prototype.id,
  CaseStudyRelations
> {

  public readonly creator: BelongsToAccessor<User, typeof CaseStudy.prototype.id>;

  public readonly mediaContent: HasOneRepositoryFactory<MediaContent, typeof CaseStudy.prototype.id>;

  constructor(
    @inject('datasources.postgres') dataSource: PostgresDataSource,
    @repository.getter('UserRepository')
    protected userRepositoryGetter: Getter<UserRepository>, @repository.getter('MediaContentRepository') protected mediaContentRepositoryGetter: Getter<MediaContentRepository>,
    ) {
    super(CaseStudy, dataSource);
    this.mediaContent = this.createHasOneRepositoryFactoryFor('mediaContent', mediaContentRepositoryGetter);
    this.registerInclusionResolver('mediaContent', this.mediaContent.inclusionResolver);
    this.creator = this.createBelongsToAccessorFor(
      'creator',
      userRepositoryGetter,
    );
    this.registerInclusionResolver('creator', this.creator.inclusionResolver);
  }

  async create(
    entity: Partial<CaseStudy> | {[P in keyof CaseStudy]?: DeepPartial<CaseStudy[P]>} | CaseStudy,
    options?: AnyObject,
  ): Promise<CaseStudy> {
    const newData = {
      ...entity,
      createdAt: moment().utc().toISOString(),
      updatedAt: moment().utc().toISOString(),
      slug: changeToSlug(entity.title),
    };

    return super.create(newData, options);
  }

  async updateById(
    id: number,
    data: DataObject<CaseStudy>,
    options?: Options,
  ): Promise<void> {
    try {
      const payload = {
        ...data,
        updatedAt: moment().utc().toISOString(),
        ... data.title ? {slug: changeToSlug(data.title),}: {}
      };
      await super.updateById(id, payload, options);
    } catch (err) {
      throw err;
    }
  }
}
