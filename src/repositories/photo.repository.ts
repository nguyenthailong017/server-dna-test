import {inject, Getter} from '@loopback/core';
import {DefaultCrudRepository, repository, HasOneRepositoryFactory} from '@loopback/repository';
import {PostgresDataSource} from '../datasources';
import {Photo, PhotoRelations, MediaContent} from '../models';
import {MediaContentRepository} from './media-content.repository';

export class PhotoRepository extends DefaultCrudRepository<
  Photo,
  typeof Photo.prototype.id,
  PhotoRelations
> {

  public readonly mediaContent: HasOneRepositoryFactory<MediaContent, typeof Photo.prototype.id>;

  constructor(
    @inject('datasources.postgres') dataSource: PostgresDataSource, @repository.getter('MediaContentRepository') protected mediaContentRepositoryGetter: Getter<MediaContentRepository>,
  ) {
    super(Photo, dataSource);
    this.mediaContent = this.createHasOneRepositoryFactoryFor('mediaContent', mediaContentRepositoryGetter);
    this.registerInclusionResolver('mediaContent', this.mediaContent.inclusionResolver);
  }
}
