import {inject, Getter} from '@loopback/core';
import {DefaultCrudRepository, repository, HasOneRepositoryFactory} from '@loopback/repository';
import {PostgresDataSource} from '../datasources';
import {Partner, PartnerRelations, MediaContent} from '../models';
import {MediaContentRepository} from './media-content.repository';

export class PartnerRepository extends DefaultCrudRepository<
  Partner,
  typeof Partner.prototype.id,
  PartnerRelations
> {

  public readonly mediaContent: HasOneRepositoryFactory<MediaContent, typeof Partner.prototype.id>;

  constructor(
    @inject('datasources.postgres') dataSource: PostgresDataSource, @repository.getter('MediaContentRepository') protected mediaContentRepositoryGetter: Getter<MediaContentRepository>,
  ) {
    super(Partner, dataSource);
    this.mediaContent = this.createHasOneRepositoryFactoryFor('mediaContent', mediaContentRepositoryGetter);
    this.registerInclusionResolver('mediaContent', this.mediaContent.inclusionResolver);
  }
}
