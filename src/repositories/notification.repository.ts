import {inject} from '@loopback/core';
import {
  AnyObject,
  DataObject,
  DeepPartial,
  DefaultCrudRepository,
  Options,
} from '@loopback/repository';
import moment from 'moment';
import {PostgresDataSource} from '../datasources';
import {Notification, NotificationRelations} from '../models';

export class NotificationRepository extends DefaultCrudRepository<
  Notification,
  typeof Notification.prototype.id,
  NotificationRelations
> {
  constructor(@inject('datasources.postgres') dataSource: PostgresDataSource) {
    super(Notification, dataSource);
  }

  async create(
    entity:
      | Partial<Notification>
      | {[P in keyof Notification]?: DeepPartial<Notification[P]>}
      | Notification,
    options?: AnyObject,
  ): Promise<Notification> {
    const newData = {
      ...entity,
      createdAt: moment().utc().toISOString(),
      updatedAt: moment().utc().toISOString(),
    };

    return super.create(newData, options);
  }

  async updateById(
    id: number,
    data: DataObject<Notification>,
    options?: Options,
  ): Promise<void> {
    try {
      const payload = {
        ...data,
        updatedAt: moment().utc().toISOString(),
      };
      await super.updateById(id, payload, options);
    } catch (err) {
      throw err;
    }
  }
}
