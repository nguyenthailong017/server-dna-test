import {inject, Getter} from '@loopback/core';
import {
  DefaultCrudRepository,
  repository,
  BelongsToAccessor,
  DataObject,
  Options,
} from '@loopback/repository';
import {PostgresDataSource} from '../datasources';
import {Teacher, TeacherRelations, User} from '../models';
import {UserRepository} from './user.repository';

export class TeacherRepository extends DefaultCrudRepository<
  Teacher,
  typeof Teacher.prototype.id,
  TeacherRelations
> {
  public readonly user: BelongsToAccessor<User, typeof Teacher.prototype.id>;

  constructor(
    @inject('datasources.postgres') dataSource: PostgresDataSource,
    @repository.getter('UserRepository')
    protected userRepositoryGetter: Getter<UserRepository>,
  ) {
    super(Teacher, dataSource);
    this.user = this.createBelongsToAccessorFor('user', userRepositoryGetter);
    this.registerInclusionResolver('user', this.user.inclusionResolver);
  }

  async updateById(
    id: number,
    data: DataObject<Teacher>,
    options?: Options,
  ): Promise<any> {
    try {
      await super.updateById(id, data, options);

      return await super.findById(id);
    } catch (err) {
      throw err;
    }
  }
}
