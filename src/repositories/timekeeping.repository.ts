import {inject, Getter} from '@loopback/core';
import {
  DefaultCrudRepository,
  repository,
  BelongsToAccessor,
  DeepPartial,
  AnyObject,
  DataObject,
} from '@loopback/repository';
import moment from 'moment';
import {PostgresDataSource} from '../datasources';
import {Staff, Timekeeping, TimekeepingRelations} from '../models';
import {StaffRepository} from './staff.repository';

export class TimekeepingRepository extends DefaultCrudRepository<
  Timekeeping,
  typeof Timekeeping.prototype.id,
  TimekeepingRelations
> {
  public readonly staff: BelongsToAccessor<
    Staff,
    typeof Timekeeping.prototype.id
  >;

  constructor(
    @inject('datasources.postgres') dataSource: PostgresDataSource,
    @repository.getter('StaffRepository')
    protected staffRepositoryGetter: Getter<StaffRepository>,
  ) {
    super(Timekeeping, dataSource);
    this.staff = this.createBelongsToAccessorFor(
      'staff',
      staffRepositoryGetter,
    );
    this.registerInclusionResolver('staff', this.staff.inclusionResolver);
  }

  async create(
    entity:
      | Partial<Timekeeping>
      | {[P in keyof Timekeeping]?: DeepPartial<Timekeeping[P]>}
      | Timekeeping,
    options?: AnyObject,
  ): Promise<Timekeeping> {
    const newData = {
      ...entity,
      createdAt: moment().utc().toISOString(),
    };

    return super.create(newData, options);
  }

  async updateById(
    id: number,
    data: DataObject<Timekeeping>,
    options?: AnyObject,
  ): Promise<void> {
    try {
      const payload = {
        ...data,
        updatedAt: moment().utc().toISOString(),
      };

      await super.updateById(id, payload, options);
    } catch (error) {
      throw error;
    }
  }
}
