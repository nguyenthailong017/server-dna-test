import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {PostgresDataSource} from '../datasources';
import {Document, DocumentRelations} from '../models';

export class DocumentRepository extends DefaultCrudRepository<
  Document,
  typeof Document.prototype.id,
  DocumentRelations
> {
  constructor(@inject('datasources.postgres') dataSource: PostgresDataSource) {
    super(Document, dataSource);
  }
}
