import {Getter, inject} from '@loopback/core';
import {
  BelongsToAccessor,
  DataObject,
  DeepPartial,
  DefaultCrudRepository,
  repository, HasManyRepositoryFactory, HasOneRepositoryFactory} from '@loopback/repository';
import {Options} from '@loopback/repository/src/common-types';
import moment from 'moment';
import {PostgresDataSource} from '../datasources';
import {Courses, CoursesRelations, User, MediaContent} from '../models';
import {changeToSlug} from '../utils';
import {UserRepository} from './user.repository';
import {MediaContentRepository} from './media-content.repository';

export class CoursesRepository extends DefaultCrudRepository<
  Courses,
  typeof Courses.prototype.id,
  CoursesRelations
> {
  public readonly creator: BelongsToAccessor<User, typeof Courses.prototype.id>;

  public readonly mediaContents: HasManyRepositoryFactory<MediaContent, typeof Courses.prototype.id>;

  public readonly courseBanner: HasOneRepositoryFactory<MediaContent, typeof Courses.prototype.id>;

  constructor(
    @inject('datasources.postgres') dataSource: PostgresDataSource,
    @repository.getter('UserRepository')
    protected userRepositoryGetter: Getter<UserRepository>, @repository.getter('MediaContentRepository') protected mediaContentRepositoryGetter: Getter<MediaContentRepository>,
  ) {
    super(Courses, dataSource);
    this.courseBanner = this.createHasOneRepositoryFactoryFor('courseBanner', mediaContentRepositoryGetter);
    this.registerInclusionResolver('courseBanner', this.courseBanner.inclusionResolver);
    this.mediaContents = this.createHasManyRepositoryFactoryFor('mediaContents', mediaContentRepositoryGetter,);
    this.registerInclusionResolver('mediaContents', this.mediaContents.inclusionResolver);
    this.creator = this.createBelongsToAccessorFor(
      'creator',
      userRepositoryGetter,
    );
    this.registerInclusionResolver('creator', this.creator.inclusionResolver);
  }

  async create(
    entity:
      | Partial<Courses>
      | {[P in keyof Courses]?: DeepPartial<Courses[P]>}
      | Courses,
    options?: Options,
  ): Promise<Courses> {
    const newData = {
      ...entity,
      createdAt: moment().utc().toISOString(),
      updatedAt: moment().utc().toISOString(),
      slug: changeToSlug(entity.title),
    };

    return super.create(newData, options);
  }

  async updateById(
    id: number,
    data: DataObject<Courses>,
    options?: Options,
  ): Promise<void> {
    try {
      const payload = {
        ...data,
        updatedAt: moment().utc().toISOString(),
        ...(data.title ? {slug: changeToSlug(data.title)} : {}),
      };
      await super.updateById(id, payload, options);
    } catch (err) {
      throw err;
    }
  }
}
