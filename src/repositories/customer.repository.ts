import {inject, Getter} from '@loopback/core';
import {
  AnyObject,
  DataObject,
  DeepPartial,
  DefaultCrudRepository,
  Options, repository, BelongsToAccessor} from '@loopback/repository';
import moment from 'moment';
import {PostgresDataSource} from '../datasources';
import {Customer, CustomerRelations, User} from '../models';
import {changeToSlug} from '../utils';
import {UserRepository} from './user.repository';

export class CustomerRepository extends DefaultCrudRepository<
  Customer,
  typeof Customer.prototype.id,
  CustomerRelations
> {

  public readonly staff: BelongsToAccessor<User, typeof Customer.prototype.id>;

  constructor(@inject('datasources.postgres') dataSource: PostgresDataSource, @repository.getter('UserRepository') protected userRepositoryGetter: Getter<UserRepository>,) {
    super(Customer, dataSource);
    this.staff = this.createBelongsToAccessorFor('staff', userRepositoryGetter,);
    this.registerInclusionResolver('staff', this.staff.inclusionResolver);
  }

  async create(
    entity:
      | Partial<Customer>
      | {[P in keyof Customer]?: DeepPartial<Customer[P]>}
      | Customer,
    options?: AnyObject,
  ): Promise<Customer> {
    const newData = {
      ...entity,
      createdAt: moment().utc().toISOString(),
      updatedAt: moment().utc().toISOString(),
      ...(entity?.firstName && entity?.lastName
        ? {
            slug: `${changeToSlug(entity?.firstName)}-${changeToSlug(
              entity?.lastName,
            )}`,
          }
        : ''),
    };

    return super.create(newData, options);
  }

  async updateById(
    id: number,
    data: DataObject<Customer>,
    options?: Options,
  ): Promise<void> {
    try {
      const payload = {
        ...data,
        updatedAt: moment().utc().toISOString(),
        ...(data?.firstName && data?.lastName
          ? {
              slug: `${changeToSlug(data?.firstName)}-${changeToSlug(
                data?.lastName,
              )}`,
            }
          : ''),
      };
      await super.updateById(id, payload, options);
    } catch (err) {
      throw err;
    }
  }
}
