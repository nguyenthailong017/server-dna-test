import {authenticate} from '@loopback/authentication';
import {authorize} from '@loopback/authorization';
import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  del,
  requestBody,
  response,
} from '@loopback/rest';
import {AUTHORIZE_ALL} from '../constants';
import {Contact} from '../models';
import {ContactRepository} from '../repositories';
import {OPERATION_SECURITY_SPEC} from '../utils';

export class ContactController {
  constructor(
    @repository(ContactRepository)
    public contactRepository: ContactRepository,
  ) {}

  @post('/contacts')
  @response(200, {
    description: 'Contact model instance',
    content: {'application/json': {schema: getModelSchemaRef(Contact)}},
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Contact, {
            title: 'NewContact',
            exclude: ['id'],
          }),
        },
      },
    })
    contact: Omit<Contact, 'id'>,
  ): Promise<Contact> {
    return this.contactRepository.create(contact);
  }

  @get('/contacts/count', {
    responses: {
      '200': {
        description: 'Booking model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(@param.where(Contact) where?: Where<Contact>): Promise<Count> {
    return this.contactRepository.count(where);
  }

  @get('/contacts', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'Array of Contact model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(Contact, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  @authenticate('jwt')
  @authorize(AUTHORIZE_ALL)
  async find(
    @param.filter(Contact) filter?: Filter<Contact>,
  ): Promise<{data: Contact[]; total: number}> {
    const [data, count] = await Promise.all([
      this.contactRepository.find({
        ...filter,
        limit: filter?.limit || 10,
      }),
      this.contactRepository.count(filter?.where),
    ]);

    return {
      data,
      total: count?.count || 0,
    };
  }

  @get('/contacts/{id}', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'Contact model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(Contact, {includeRelations: true}),
          },
        },
      },
    },
  })
  @authenticate('jwt')
  @authorize(AUTHORIZE_ALL)
  async findById(
    @param.path.number('id') id: number,
    @param.filter(Contact, {exclude: 'where'})
    filter?: FilterExcludingWhere<Contact>,
  ): Promise<Contact> {
    return this.contactRepository.findById(id, filter);
  }

  @patch('/contacts/{id}', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '204': {
        description: 'Contact PATCH success',
      },
    },
  })
  @authenticate('jwt')
  @authorize(AUTHORIZE_ALL)
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Contact, {partial: true}),
        },
      },
    })
    contact: Contact,
  ): Promise<void> {
    await this.contactRepository.updateById(id, contact);
  }

  @del('/contacts/{id}', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '204': {
        description: 'Contact DELETE success',
      },
    },
  })
  @authenticate('jwt')
  @authorize(AUTHORIZE_ALL)
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.contactRepository.deleteById(id);
  }
}
