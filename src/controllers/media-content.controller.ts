import {Filter, FilterExcludingWhere, repository} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  del,
  requestBody,
  RestBindings,
  Request,
  Response,
  patch,
} from '@loopback/rest';
import {MediaContent} from '../models';
import {
  MediaContentRepository,
  NewsRepository,
  ReviewRepository,
} from '../repositories';
import {FileUploadController} from './file.controller';
import {inject} from '@loopback/context';
import {authenticate} from '@loopback/authentication';
import {authorize} from '@loopback/authorization';
import {AUTHORIZE_ALL, MEDIA_TYPE_NEWS} from '../constants';
import {OPERATION_SECURITY_SPEC} from '@loopback/authentication-jwt';

export class MediaContentController {
  constructor(
    @repository(MediaContentRepository)
    public mediaContentRepository: MediaContentRepository,
    @repository(NewsRepository)
    public newsRepository: NewsRepository,
    @repository(ReviewRepository)
    public reviewRepository: ReviewRepository,
    @inject('controllers.FileUploadController')
    private fileUploadController: FileUploadController,
  ) {}

  @post('/media-contents/news/{newsId}', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'MediaContent model instance',
        content: {
          'application/json': {schema: getModelSchemaRef(MediaContent)},
        },
      },
    },
  })
  @authenticate('jwt')
  @authorize(AUTHORIZE_ALL)
  async newsCreate(
    @param.path.number('newsId') newsId: number,
    @requestBody.file({
      required: true,
      description: 'Upload file',
    })
    request: Request,
    @inject(RestBindings.Http.RESPONSE) response: Response,
  ) {
    try {
      const files: any = await this.fileUploadController.uploadFiles(
        request,
        response,
      );

      const result: MediaContent[] = await Promise.all(
        files.map(async (file: any) => {
          return this.newsRepository.mediaContents(newsId).create({
            ...file,
            sourceType: MEDIA_TYPE_NEWS,
          });
        }),
      );

      return result;
    } catch (e) {
      return e;
    }
  }

  @patch('/media-contents/{id}', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '204': {
        description: 'MediaContent PATCH success',
      },
    },
  })
  @authenticate('jwt')
  @authorize(AUTHORIZE_ALL)
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: {
            type: 'object',
            title: 'UpdateNews',
            properties: {
              status: {
                type: 'string',
              },
            },
          },
        },
      },
    })
    mediaContent: MediaContent,
  ): Promise<MediaContent> {
    await this.mediaContentRepository.updateById(id, mediaContent);

    return this.mediaContentRepository.findById(id);
  }

  @get('/media-contents', {
    responses: {
      '200': {
        description: 'Array of MediaContent model instances',
        content: {
          'application/json': {
            schema: {
              title: 'listPhotos',
              type: 'object',
              properties: {
                total: {
                  type: 'number',
                },
                data: {
                  type: 'array',
                  items: getModelSchemaRef(MediaContent, {
                    includeRelations: true,
                  }),
                },
              },
            },
          },
        },
      },
    },
  })
  async getPhotos(
    @param.filter(MediaContent) filter?: Filter<MediaContent>,
  ): Promise<{data: MediaContent[]; total: number}> {
    const [data, count] = await Promise.all([
      this.mediaContentRepository.find({
        ...filter,
      }),
      this.mediaContentRepository.count({
        ...(filter?.where ? filter?.where : {}),
      }),
    ]);

    return {
      data,
      total: count.count || 0,
    };
  }

  @get('/media-contents/{id}', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'MediaContent model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(MediaContent, {includeRelations: true}),
          },
        },
      },
    },
  })
  @authenticate('jwt')
  @authorize(AUTHORIZE_ALL)
  async findById(
    @param.path.number('id') id: number,
    @param.filter(MediaContent, {exclude: 'where'})
    filter?: FilterExcludingWhere<MediaContent>,
  ): Promise<MediaContent> {
    return this.mediaContentRepository.findById(id, filter);
  }

  @del('/media-contents/{id}', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '204': {
        description: 'MediaContent DELETE success',
      },
    },
  })
  @authenticate('jwt')
  @authorize(AUTHORIZE_ALL)
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.mediaContentRepository.deleteById(id);
  }
}
