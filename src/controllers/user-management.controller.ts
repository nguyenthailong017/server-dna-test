// Copyright IBM Corp. 2020. All Rights Reserved.
// Node module: loopback4-example-shopping
// This file is licensed under the MIT License.
// License text available at https://opensource.org/licenses/MIT

import {
  authenticate,
  TokenService,
  UserService,
} from '@loopback/authentication';
import {TokenServiceBindings} from '@loopback/authentication-jwt';
import {authorize} from '@loopback/authorization';
import {inject} from '@loopback/core';
import {
  AnyObject,
  Count,
  CountSchema,
  Filter,
  model,
  property,
  repository,
  Where,
} from '@loopback/repository';
import {
  del,
  get,
  getModelSchemaRef,
  HttpErrors,
  param,
  patch,
  post,
  put,
  requestBody,
} from '@loopback/rest';
import {SecurityBindings, securityId, UserProfile} from '@loopback/security';
import _ from 'lodash';
import {PasswordHasherBindings, UserServiceBindings} from '../keys';
import {Staff, Student, Teacher, User} from '../models';
import {
  Credentials,
  StaffRepository,
  StudentRepository,
  TeacherRepository,
  UserRepository,
} from '../repositories';
import {
  PasswordHasher,
  UserManagementService,
  validateCredentials,
  validateRoles,
} from '../services';
import {OPERATION_SECURITY_SPEC} from '../utils';
import {
  CredentialsRequestBody,
  PasswordResetRequestBody,
  UserProfileSchema,
} from './specs/user-controller.specs';
import {AUTHORIZE_ADMIN, AUTHORIZE_ALL} from '../constants';
import {omit} from 'ramda';
import moment from 'moment';
@model()
export class NewUserRequest extends User {
  @property({
    type: 'string',
    required: true,
  })
  password: string;

  @property({
    type: 'object',
  })
  staff: Staff;
  @property({
    type: 'object',
  })
  student: Student;
  @property({
    type: 'object',
  })
  teacher: Teacher;
}

@model()
export class UpdateUserRequest extends User {
  @property({
    type: 'string',
  })
  password?: string;
}

export class UserManagementController {
  constructor(
    @repository(UserRepository)
    public userRepository: UserRepository,
    @repository(StaffRepository)
    public staffRepository: StaffRepository,
    @repository(StaffRepository)
    public studentRepository: StudentRepository,
    @repository(StudentRepository)
    public teacherRepository: TeacherRepository,
    @inject(PasswordHasherBindings.PASSWORD_HASHER)
    public passwordHasher: PasswordHasher,
    @inject(TokenServiceBindings.TOKEN_SERVICE)
    public jwtService: TokenService,
    @inject(UserServiceBindings.USER_SERVICE)
    public userService: UserService<User, Credentials>,
    @inject(UserServiceBindings.USER_SERVICE)
    public userManagementService: UserManagementService,
  ) {}

  @post('/users', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'User',
        content: {
          'application/json': {
            schema: {
              'x-ts-type': User,
            },
          },
        },
      },
    },
  })
  @authenticate('jwt')
  @authorize(AUTHORIZE_ADMIN)
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(NewUserRequest, {
            title: 'NewUser',
          }),
        },
      },
    })
    newUserRequest: NewUserRequest,
  ): Promise<AnyObject | undefined> {
    // All new users have the "customer" role by default
    // newUserRequest.roles = ['customer'];
    // ensure a valid email value and password value
    validateCredentials(_.pick(newUserRequest, ['email', 'password']));
    validateRoles(newUserRequest);

    try {
      newUserRequest.resetKey = '';

      if (newUserRequest.student) {
        const student = newUserRequest.student;
        const payload: any = omit(['student'], newUserRequest);
        const resUser = await this.userManagementService.createUser(payload);
        const resStudent = await this.studentRepository.create({
          ...student,
          userId: resUser?.id,
        });

        return {
          ...resUser,
          staff: resStudent,
        };
      }
      if (newUserRequest.teacher) {
        const teacher = newUserRequest.teacher;
        const payload: any = omit(['teacher'], newUserRequest);
        const resUser = await this.userManagementService.createUser(payload);
        const resTeacher = await this.studentRepository.create({
          ...teacher,
          userId: resUser?.id,
        });

        return {
          ...resUser,
          staff: resTeacher,
        };
      }

      return await this.userManagementService.createUser(newUserRequest);
    } catch (error) {
      // MongoError 11000 duplicate key
      if (error.code === 11000 && error.errmsg.includes('index: uniqueEmail')) {
        throw new HttpErrors.Conflict('Email value is already taken');
      } else {
        throw error;
      }
    }
  }

  @get('/users/count', {
    responses: {
      '200': {
        description: 'ChangeSchedule model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(@param.where(User) where?: Where<User>): Promise<Count> {
    return this.userRepository.count(where);
  }

  @patch('/admin/users/{userId}', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '204': {
        description: 'User PATCH success',
      },
    },
  })
  @authenticate('jwt')
  @authorize(AUTHORIZE_ADMIN)
  async set(
    @param.path.string('userId') userId: number,
    @requestBody({
      description: 'update user',
      content: {
        'application/json': {
          schema: getModelSchemaRef(UpdateUserRequest, {
            title: 'UpdateUser',
            exclude: ['id'],
          }),
        },
      },
    })
    user: Omit<UpdateUserRequest, 'id'>,
  ): Promise<void> {
    try {
      let newUser: any = user;

      if (newUser.password) {
        const password = await this.passwordHasher.hashPassword(
          newUser.password,
        );

        await this.userRepository
          .userCredentials(userId)
          .patch({password: password});

        newUser = omit(['password'], newUser);
      }

      return await this.userRepository.updateById(userId, newUser);
    } catch (e) {
      return e;
    }
  }

  @patch('/users/{userId}', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '204': {
        description: 'User PATCH success',
      },
    },
  })
  @authenticate('jwt')
  @authorize(AUTHORIZE_ALL)
  async editProfile(
    @inject(SecurityBindings.USER)
    currentUserProfile: UserProfile,
    @param.path.string('userId') userId: number,
    @requestBody({
      description: 'update user',
      content: {
        'application/json': {
          schema: {
            type: 'object',
            title: 'newCheckInLocation',
            properties: {
              email: {
                type: 'string',
              },
              firstName: {
                type: 'string',
              },
              lastName: {
                type: 'string',
              },
              phone: {
                type: 'string',
              },
              birthday: {
                type: 'string',
              },
              class: {
                type: 'string',
              },
              gender: {
                type: 'string',
              },
              password: {
                type: 'string',
              },
            },
            required: [],
          },
        },
      },
    })
    user: Omit<UpdateUserRequest, 'id'>,
  ): Promise<void> {
    const userRequestId: any = currentUserProfile[securityId];
    if (userRequestId !== userId) {
      throw new HttpErrors.Unauthorized('You are not owner of this account');
    }
    try {
      let newUser: any = user;

      if (newUser.password) {
        const password = await this.passwordHasher.hashPassword(
          newUser.password,
        );

        await this.userRepository
          .userCredentials(userId)
          .patch({password: password});

        newUser = omit(['password'], newUser);
      }

      return await this.userRepository.updateById(userId, newUser);
    } catch (e) {
      return e;
    }
  }

  @get('/users', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'Array of User model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(User, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  @authenticate('jwt')
  @authorize(AUTHORIZE_ALL)
  async find(
    @param.filter(User) filter?: Filter<User>,
  ): Promise<{data: User[]; total: number}> {
    const [data, count] = await Promise.all([
      this.userRepository.find({
        limit: filter?.limit || 10,
        ...filter,
      }),
      this.userRepository.count(filter?.where),
    ]);

    return {
      data,
      total: count?.count || 0,
    };
  }

  @get('/users/{userId}', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'User',
        content: {
          'application/json': {
            schema: {
              'x-ts-type': User,
            },
          },
        },
      },
    },
  })
  @authenticate('jwt')
  @authorize(AUTHORIZE_ADMIN)
  async findById(@param.path.number('userId') userId: number): Promise<User> {
    return this.userRepository.findById(userId);
  }

  @get('/users/me', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'The current user profile',
        content: {
          'application/json': {
            schema: UserProfileSchema,
          },
        },
      },
    },
  })
  @authenticate('jwt')
  async printCurrentUser(
    @inject(SecurityBindings.USER)
    currentUserProfile: UserProfile,
  ): Promise<User> {
    // (@jannyHou)FIXME: explore a way to generate OpenAPI schema
    // for symbol property

    const userId: any = currentUserProfile[securityId];
    return this.userRepository.findById(userId);
  }

  @post('/users/login', {
    responses: {
      '200': {
        description: 'Token',
        content: {
          'application/json': {
            schema: {
              type: 'object',
              properties: {
                token: {
                  type: 'string',
                },
              },
            },
          },
        },
      },
    },
  })
  async login(
    @requestBody(CredentialsRequestBody) credentials: Credentials,
  ): Promise<{token: string}> {
    // ensure the user exists, and the password is correct
    const user = await this.userService.verifyCredentials(credentials);

    // convert a User object into a UserProfile object (reduced set of properties)
    const userProfile = this.userService.convertToUserProfile(user);

    // create a JSON Web Token based on the user profile
    const token = await this.jwtService.generateToken(userProfile);

    return {token};
  }

  @put('/users/forgot-password', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'The updated user profile',
        content: {
          'application/json': {
            schema: UserProfileSchema,
          },
        },
      },
    },
  })
  @authenticate('jwt')
  async forgotPassword(
    @inject(SecurityBindings.USER)
    currentUserProfile: UserProfile,
    @requestBody(PasswordResetRequestBody) credentials: Credentials,
  ): Promise<{token: string}> {
    const {email, password} = credentials;
    const {id} = currentUserProfile;

    const user = await this.userRepository.findById(id);

    if (!user) {
      throw new HttpErrors.NotFound('User account not found');
    }

    if (email !== user?.email) {
      throw new HttpErrors.Forbidden('Invalid email address');
    }

    validateCredentials(_.pick(credentials, ['email', 'password']));

    const passwordHash = await this.passwordHasher.hashPassword(password);

    await this.userRepository
      .userCredentials(user.id)
      .patch({password: passwordHash});

    const userProfile = this.userService.convertToUserProfile(user);

    const token = await this.jwtService.generateToken(userProfile);

    return {token};
  }

  @patch('/admin/users/lock/{userId}', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '204': {
        description: 'User PATCH success',
      },
    },
  })
  @authenticate('jwt')
  @authorize(AUTHORIZE_ADMIN)
  async blockUser(
    @param.path.string('userId') userId: number,
    @requestBody({
      description: 'block user',
      content: {
        'application/json': {
          schema: {
            type: 'object',
            title: 'LockUser',
            properties: {
              lockMessage: {
                type: 'string',
              },
            },
          },
        },
      },
    })
    {
      lockMessage,
    }: {
      lockMessage: string;
    },
  ): Promise<{message: string}> {
    try {
      const user = await this.userRepository.findById(userId, {
        fields: ['id', 'lockedAt'],
      });
      if (lockMessage) {
        if (user.lockedAt) {
          throw new HttpErrors.NotFound('User has been blocked');
        }

        await this.userRepository.updateById(userId, {
          lockMessage,
          lockedAt: moment().utc().toISOString(),
        });
        return {
          message: 'Lock user is successfully!!',
        };
      }

      if (!user.lockedAt) {
        throw new HttpErrors.NotFound('User has been unblocked');
      }

      await this.userRepository.updateById(userId, {
        lockedAt: null,
        lockMessage: '',
      });
      return {
        message: 'Unlock user is successfully!!',
      };
    } catch (e) {
      return e;
    }
  }

  @del('/admin/users/{userId}', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '204': {
        description: 'User DELETE success',
      },
    },
  })
  @authenticate('jwt')
  @authorize(AUTHORIZE_ADMIN)
  async deletedUser(
    @param.path.string('userId') userId: number,
  ): Promise<{message: string}> {
    try {
      const user = await this.userRepository.findById(userId, {
        fields: ['id'],
      });
      if (!user.id) {
        throw new HttpErrors.NotFound('User has been deleted');
      }

      await this.userRepository.deleteById(userId);
      return {
        message: 'delete user is successfully!!',
      };
    } catch (e) {
      return e;
    }
  }
}
