import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
  response,
} from '@loopback/rest';
import {StudentReview} from '../models';
import {StudentReviewRepository} from '../repositories';

export class StudentReviewController {
  constructor(
    @repository(StudentReviewRepository)
    public studentReviewRepository : StudentReviewRepository,
  ) {}

  @post('/student-reviews')
  @response(200, {
    description: 'StudentReview model instance',
    content: {'application/json': {schema: getModelSchemaRef(StudentReview)}},
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(StudentReview, {
            title: 'NewStudentReview',
            exclude: ['id'],
          }),
        },
      },
    })
    studentReview: Omit<StudentReview, 'id'>,
  ): Promise<StudentReview> {
    return this.studentReviewRepository.create(studentReview);
  }

  @get('/student-reviews/count')
  @response(200, {
    description: 'StudentReview model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(
    @param.where(StudentReview) where?: Where<StudentReview>,
  ): Promise<Count> {
    return this.studentReviewRepository.count(where);
  }

  @get('/student-reviews')
  @response(200, {
    description: 'Array of StudentReview model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(StudentReview, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.filter(StudentReview) filter?: Filter<StudentReview>,
  ): Promise<StudentReview[]> {
    return this.studentReviewRepository.find(filter);
  }

  @patch('/student-reviews')
  @response(200, {
    description: 'StudentReview PATCH success count',
    content: {'application/json': {schema: CountSchema}},
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(StudentReview, {partial: true}),
        },
      },
    })
    studentReview: StudentReview,
    @param.where(StudentReview) where?: Where<StudentReview>,
  ): Promise<Count> {
    return this.studentReviewRepository.updateAll(studentReview, where);
  }

  @get('/student-reviews/{id}')
  @response(200, {
    description: 'StudentReview model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(StudentReview, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(StudentReview, {exclude: 'where'}) filter?: FilterExcludingWhere<StudentReview>
  ): Promise<StudentReview> {
    return this.studentReviewRepository.findById(id, filter);
  }

  @patch('/student-reviews/{id}')
  @response(204, {
    description: 'StudentReview PATCH success',
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(StudentReview, {partial: true}),
        },
      },
    })
    studentReview: StudentReview,
  ): Promise<void> {
    await this.studentReviewRepository.updateById(id, studentReview);
  }

  @put('/student-reviews/{id}')
  @response(204, {
    description: 'StudentReview PUT success',
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() studentReview: StudentReview,
  ): Promise<void> {
    await this.studentReviewRepository.replaceById(id, studentReview);
  }

  @del('/student-reviews/{id}')
  @response(204, {
    description: 'StudentReview DELETE success',
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.studentReviewRepository.deleteById(id);
  }
}
