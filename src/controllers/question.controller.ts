import {OPERATION_SECURITY_SPEC} from '@loopback/authentication-jwt';
import {inject} from '@loopback/core';
import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  IsolationLevel,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  del,
  requestBody,
  response,
  HttpErrors,
} from '@loopback/rest';
import {QuestionRequestInterface} from '../constants/question.constant';
import {Question} from '../models';
import {AnswerRepository, QuestionRepository} from '../repositories';
import {SecurityBindings, securityId, UserProfile} from '@loopback/security';
import {authenticate} from '@loopback/authentication';
import {AUTHORIZE_ADMIN} from '../constants';
import {authorize} from '@loopback/authorization';

export class QuestionController {
  constructor(
    @repository(QuestionRepository)
    public questionRepository: QuestionRepository,
    @repository(AnswerRepository)
    public answerRepository: AnswerRepository,
  ) {}

  @post('/questions', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'Question model instance',
        content: {'application/json': {schema: getModelSchemaRef(Question)}},
      },
    },
  })
  @authenticate('jwt')
  @authorize(AUTHORIZE_ADMIN)
  async create(
    @inject(SecurityBindings.USER) currentUserProfile: UserProfile,
    @requestBody({
      content: {
        'application/json': {
          schema: {
            type: 'object',
            title: 'NewQuestion',
            properties: {
              ...getModelSchemaRef(Question, {
                title: 'NewQuestionExclude',
                exclude: [
                  'id',
                  'createdAt',
                  'updatedAt',
                  'answerResult',
                  'creatorId',
                ],
              }).definitions.NewQuestionExclude.properties,
              correctAnswer: {
                type: 'string',
              },
            },
          },
        },
        required: ['correctAnswer'],
      },
    })
    question: Omit<QuestionRequestInterface, 'id'>,
  ): Promise<{message: string}> {
    const transaction = await this.questionRepository.beginTransaction({
      isolationLevel: IsolationLevel.READ_COMMITTED,
      timeout: 600000,
    });
    const userId = currentUserProfile[securityId];
    try {
      const result = await this.questionRepository.create(
        {
          content: question?.content,
          answer: question?.answer,
          creatorId: Number(userId),
        },
        {transaction},
      );

      if (!result?.id)
        throw new HttpErrors.BadRequest('create question failure -_-');

      await this.answerRepository.create(
        {
          content: question?.correctAnswer,
          questionId: result.id,
          creatorId: Number(userId),
        },
        {transaction},
      );

      await transaction.commit();
      return {
        message: 'create question is successfully!!',
      };
    } catch (error) {
      await transaction.rollback();
      throw error;
    }
  }

  @get('/questions/count')
  @response(200, {
    description: 'Question model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(@param.where(Question) where?: Where<Question>): Promise<Count> {
    return this.questionRepository.count(where);
  }

  @get('/questions')
  @response(200, {
    description: 'Array of Question model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(Question, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.filter(Question) filter?: Filter<Question>,
  ): Promise<{data: Question[]; total: number}> {
    try {
      const [data, total] = await Promise.all([
        this.questionRepository.find(filter),
        this.questionRepository.count(filter?.where),
      ]);
      return {
        data,
        total: total?.count,
      };
    } catch (error) {
      return {
        data: [],
        total: 0,
      };
    }
  }

  @get('/questions/{id}')
  @response(200, {
    description: 'Question model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(Question, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(Question, {exclude: 'where'})
    filter?: FilterExcludingWhere<Question>,
  ): Promise<Question> {
    return this.questionRepository.findById(id, {
      include: [
        {
          relation: 'answerResult',
          scope: {
            fields: ['content', 'questionId'],
          },
        },
      ],
      ...filter,
    });
  }

  @patch('/questions/{id}', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '204': {
        description: 'Question PATCH success',
        content: {'application/json': {schema: getModelSchemaRef(Question)}},
      },
    },
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: {
            type: 'object',
            title: 'UpdateNewQuestion',
            properties: {
              ...getModelSchemaRef(Question, {
                title: 'UpdateNewQuestionExclude',
                exclude: [
                  'id',
                  'createdAt',
                  'updatedAt',
                  'answerResult',
                  'creatorId',
                ],
              }).definitions.UpdateNewQuestionExclude.properties,
              correctAnswer: {
                type: 'string',
              },
            },
          },
        },
        required: ['correctAnswer'],
      },
    })
    question: Omit<QuestionRequestInterface, 'id'>,
  ): Promise<Question> {
    const transaction = await this.questionRepository.beginTransaction({
      isolationLevel: IsolationLevel.READ_COMMITTED,
      timeout: 600000,
    });
    try {
      await Promise.all([
        await this.questionRepository.updateById(
          id,
          {
            content: question?.content,
            answer: question?.answer,
          },
          {transaction},
        ),
        this.answerRepository.updateAll(
          {
            content: question?.correctAnswer,
          },
          {
            questionId: id,
          },
          {transaction},
        ),
      ]);

      await transaction.commit();
      return this.questionRepository.findById(id);
    } catch (error) {
      await transaction.rollback();
      throw error;
    }
  }

  @del('/questions/{id}', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '204': {
        description: 'Question DELETE success',
        content: {'application/json': {schema: getModelSchemaRef(Question)}},
      },
    },
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.questionRepository.deleteById(id);
  }
}
