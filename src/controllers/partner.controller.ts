import {Filter, FilterExcludingWhere, repository} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  del,
  requestBody,
  response,
} from '@loopback/rest';
import {Partner} from '../models';
import {PartnerRepository} from '../repositories';

export class PartnerController {
  constructor(
    @repository(PartnerRepository)
    public partnerRepository: PartnerRepository,
  ) {}

  @post('/partners')
  @response(200, {
    description: 'Partner model instance',
    content: {'application/json': {schema: getModelSchemaRef(Partner)}},
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Partner, {
            title: 'NewPartner',
            exclude: ['id'],
          }),
        },
      },
    })
    partner: Omit<Partner, 'id'>,
  ): Promise<Partner> {
    return this.partnerRepository.create(partner);
  }

  @get('/partners')
  @response(200, {
    description: 'Array of Partner model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(Partner, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.filter(Partner) filter?: Filter<Partner>,
  ): Promise<Partner[]> {
    return this.partnerRepository.find(filter);
  }

  @get('/partners/{id}')
  @response(200, {
    description: 'Partner model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(Partner, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(Partner, {exclude: 'where'})
    filter?: FilterExcludingWhere<Partner>,
  ): Promise<Partner> {
    return this.partnerRepository.findById(id, filter);
  }

  @patch('/partners/{id}')
  @response(204, {
    description: 'Partner PATCH success',
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Partner, {partial: true}),
        },
      },
    })
    partner: Partner,
  ): Promise<void> {
    await this.partnerRepository.updateById(id, partner);
  }

  @del('/partners/{id}')
  @response(204, {
    description: 'Partner DELETE success',
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.partnerRepository.deleteById(id);
  }
}
