import { inject } from '@loopback/core';
import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  del,
  get,
  getModelSchemaRef,
  getWhereSchemaFor,
  param,
  patch,
  post,
  requestBody,
  RestBindings,
  Response,
  Request,
} from '@loopback/rest';
import {
  Courses,
  MediaContent,
} from '../models';
import {CoursesRepository} from '../repositories';
import { FileUploadController } from './file.controller';

export class CoursesMediaContentController {
  constructor(
    @repository(CoursesRepository) protected coursesRepository: CoursesRepository,
    @inject('controllers.FileUploadController')
    private fileUploadController: FileUploadController,
  ) { }

  @get('/courses/{id}/media-contents', {
    responses: {
      '200': {
        description: 'Array of Courses has many MediaContent',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(MediaContent)},
          },
        },
      },
    },
  })
  async find(
    @param.path.number('id') id: number,
    @param.query.object('filter') filter?: Filter<MediaContent>,
  ): Promise<MediaContent[]> {
    return this.coursesRepository.mediaContents(id).find(filter);
  }

  @post('/courses/{id}/media-contents', {
    responses: {
      '200': {
        description: 'Courses model instance',
        content: {'application/json': {schema: getModelSchemaRef(MediaContent)}},
      },
    },
  })
  async create(
    @param.path.number('id') id: typeof Courses.prototype.id,
    @requestBody.file({
      required: true,
      description: 'Upload file',
    })
    request: Request,
    @inject(RestBindings.Http.RESPONSE) response: Response,
  ): Promise<MediaContent[]> {
    const files: any = await this.fileUploadController.uploadFiles(
      request,
      response,
    );

    const result: MediaContent[] = await Promise.all(
      files.map(async (file: any) => {
        return this.coursesRepository.mediaContents(id).create(file);
      }),
    );

    return result;
  }

  @patch('/courses/{id}/media-contents', {
    responses: {
      '200': {
        description: 'Courses.MediaContent PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async patch(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(MediaContent, {partial: true}),
        },
      },
    })
    mediaContent: Partial<MediaContent>,
    @param.query.object('where', getWhereSchemaFor(MediaContent)) where?: Where<MediaContent>,
  ): Promise<Count> {
    return this.coursesRepository.mediaContents(id).patch(mediaContent, where);
  }

  @del('/courses/{id}/media-contents', {
    responses: {
      '200': {
        description: 'Courses.MediaContent DELETE success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async delete(
    @param.path.number('id') id: number,
    @param.query.object('where', getWhereSchemaFor(MediaContent)) where?: Where<MediaContent>,
  ): Promise<Count> {
    return this.coursesRepository.mediaContents(id).delete(where);
  }
}
