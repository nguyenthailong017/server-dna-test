import {authenticate} from '@loopback/authentication';
import {OPERATION_SECURITY_SPEC} from '@loopback/authentication-jwt';
import {authorize} from '@loopback/authorization';
import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  del,
  requestBody,
} from '@loopback/rest';
import {
  AUTHORIZE_ALL,
  MEDIA_TYPE_REVIEW,
  MEDIA_TYPE_REVIEW_AVATAR,
} from '../constants';
import {MediaContent, Review, ReviewWithRelations} from '../models';
import {ReviewRepository, MediaContentRepository} from '../repositories';

export class ReviewController {
  constructor(
    @repository(ReviewRepository)
    public reviewRepository: ReviewRepository,
    @repository(MediaContentRepository)
    public mediaContentRepository: MediaContentRepository,
  ) {}

  @post('/reviews', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'Review model instance',
        content: {'application/json': {schema: getModelSchemaRef(Review)}},
      },
    },
  })
  @authenticate('jwt')
  @authorize(AUTHORIZE_ALL)
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Review, {
            title: 'NewReview',
            exclude: ['id'],
          }),
        },
      },
    })
    review: Omit<Review, 'id'>,
  ): Promise<Review> {
    return this.reviewRepository.create(review);
  }

  @get('/reviews/count', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'Review model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  @authenticate('jwt')
  @authorize(AUTHORIZE_ALL)
  async count(@param.where(Review) where?: Where<Review>): Promise<Count> {
    return this.reviewRepository.count(where);
  }

  @get('/reviews', {
    responses: {
      '200': {
        description: 'Array of Review model instances',
        content: {
          'application/json': {
            schema: {
              title: 'listReviews',
              type: 'object',
              properties: {
                total: {
                  type: 'number',
                },
                data: {
                  type: 'array',
                  items: getModelSchemaRef(Review, {includeRelations: true}),
                },
              },
            },
          },
        },
      },
    },
  })
  async find(
    @param.filter(Review) filter?: Filter<Review>,
  ): Promise<{data: any[]; total: number}> {
    const [data, count] = await Promise.all([
      this.reviewRepository.find(filter),
      this.reviewRepository.count(filter?.where),
    ]);

    return {
      data,
      total: count?.count || 0,
    };
  }

  @get('/reviews/{id}', {
    responses: {
      '200': {
        description: 'Review model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(Review, {includeRelations: true}),
          },
        },
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(Review, {exclude: 'where'})
    filter?: FilterExcludingWhere<Review>,
  ): Promise<any> {
    try {
      const detail: any = await this.reviewRepository.findById(id, filter);

      return detail;
    } catch (e) {
      return e;
    }
  }

  @patch('/reviews/{id}', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '204': {
        description: 'Review PATCH success',
      },
    },
  })
  @authenticate('jwt')
  @authorize(AUTHORIZE_ALL)
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Review, {partial: true}),
        },
      },
    })
    review: Review,
  ): Promise<Review> {
    await this.reviewRepository.updateById(id, review);

    return this.reviewRepository.findById(id);
  }

  @del('/reviews/{id}', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '204': {
        description: 'Review DELETE success',
      },
    },
  })
  @authenticate('jwt')
  @authorize(AUTHORIZE_ALL)
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.reviewRepository.deleteById(id);
  }
}
