import {UserService} from '@loopback/authentication';
import {inject} from '@loopback/core';
import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  del,
  requestBody,
  response,
} from '@loopback/rest';
import {pick} from 'ramda';
import {PasswordHasherBindings, UserServiceBindings} from '../keys';
import {Staff, User} from '../models';
import {Credentials, StaffRepository, UserRepository} from '../repositories';
import {
  PasswordHasher,
  UserManagementService,
  validateCredentials,
} from '../services';
import {NewUserRequest} from './user-management.controller';

export class StaffController {
  constructor(
    @repository(StaffRepository)
    public staffRepository: StaffRepository,
    @inject(UserServiceBindings.USER_SERVICE)
    public userService: UserService<User, Credentials>,
    @inject(UserServiceBindings.USER_SERVICE)
    public userManagementService: UserManagementService,
    @repository(UserRepository)
    public userRepository: UserRepository,
    @inject(PasswordHasherBindings.PASSWORD_HASHER)
    public passwordHasher: PasswordHasher,
  ) {}

  @post('/staff')
  @response(200, {
    description: 'Staff model instance',
    content: {'application/json': {schema: getModelSchemaRef(Staff)}},
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: {
            title: 'NewPost',
            type: 'object',
            properties: {
              staff: {
                type: 'object',
                properties: {
                  ...getModelSchemaRef(Staff, {
                    title: 'NewStaff',
                    exclude: ['id'],
                  }).definitions['NewStaff'].properties,
                },
              },
              user: {
                type: 'object',
                properties: {
                  ...getModelSchemaRef(NewUserRequest, {
                    title: 'NewUser',
                    exclude: ['id'],
                  }).definitions['NewUser'].properties,
                },
              },
            },
          },
        },
      },
    })
    staff: {
      user: NewUserRequest;
      staff: Staff;
    },
  ): Promise<{user: User; staff: Staff}> {
    try {
      validateCredentials(pick(['email', 'password'], staff.user));
      const resUser = await this.userManagementService.createUser(staff.user);
      const resStaff = await this.staffRepository.create({
        ...staff.staff,
        userId: resUser?.id,
      });

      return {
        user: resUser,
        staff: resStaff,
      };
    } catch (e) {
      throw e;
    }
  }

  @patch('/staff/{id}')
  @response(204, {
    description: 'Staff PATCH success',
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: {
            title: 'NewPost',
            type: 'object',
            properties: {
              staff: {
                type: 'object',
                properties: {
                  ...getModelSchemaRef(Staff, {
                    title: 'NewStaff',
                    partial: true,
                  }).definitions['NewStaff'].properties,
                },
              },
              user: {
                type: 'object',
                properties: {
                  ...getModelSchemaRef(NewUserRequest, {
                    title: 'NewUser',
                    partial: true,
                  }).definitions['NewUser'].properties,
                },
              },
            },
          },
        },
      },
    })
    staff: {
      user: NewUserRequest;
      staff: Staff;
    },
  ): Promise<{user: User; staff: Staff}> {
    try {
      const resStaff = await this.staffRepository.updateById(id, {
        ...staff.staff,
      });

      const resUser = await this.userRepository.updateById(
        resStaff?.userId,
        staff.user,
      );

      return {
        user: resUser,
        staff: resStaff,
      };
    } catch (e) {
      throw e;
    }
  }

  @get('/staff/count')
  @response(200, {
    description: 'Staff model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(@param.where(Staff) where?: Where<Staff>): Promise<Count> {
    return this.staffRepository.count(where);
  }

  @get('/staff')
  @response(200, {
    description: 'Array of Staff model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(Staff, {includeRelations: true}),
        },
      },
    },
  })
  async find(@param.filter(Staff) filter?: Filter<Staff>): Promise<Staff[]> {
    return this.staffRepository.find(filter);
  }

  @get('/staff/{id}')
  @response(200, {
    description: 'Staff model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(Staff, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(Staff, {exclude: 'where'})
    filter?: FilterExcludingWhere<Staff>,
  ): Promise<Staff> {
    return this.staffRepository.findById(id, filter);
  }

  @del('/staff/{id}')
  @response(204, {
    description: 'Staff DELETE success',
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    const user = await this.staffRepository.findById(id, {
      fields: ['id', 'userId'],
    });

    await this.staffRepository.deleteById(id);
    await this.userRepository.deleteById(user.userId);
  }
}
