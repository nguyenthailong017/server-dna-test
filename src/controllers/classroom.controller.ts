import {UserRepository} from '@loopback/authentication-jwt';
import {
  AnyObject,
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  IsolationLevel,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
  response,
  HttpErrors,
} from '@loopback/rest';
import {TRANSACTION_TIMEOUT} from '../constants';
import {Classroom} from '../models';
import {
  ClassroomRepository,
  RoomRepository,
  ScheduleRepository,
  ShiftRepository,
} from '../repositories';
import {getDatesBetween2Date} from '../utils/schedule';

export class ClassroomController {
  constructor(
    @repository(ClassroomRepository)
    public classroomRepository: ClassroomRepository,
    @repository(ScheduleRepository)
    public scheduleRepository: ScheduleRepository,
    @repository(UserRepository)
    public userRepository: UserRepository,
    @repository(ShiftRepository)
    public shiftRepository: ShiftRepository,
    @repository(RoomRepository)
    public roomRepository: RoomRepository,
  ) {}

  @post('/classrooms')
  @response(200, {
    description: 'Classroom model instance',
    content: {'application/json': {schema: getModelSchemaRef(Classroom)}},
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Classroom, {
            title: 'NewClassroom',
            exclude: ['id'],
          }),
        },
      },
    })
    classroom: Omit<Classroom, 'id'>,
  ): Promise<Classroom> {
    try {
      const {schedule, shift, startDate} = classroom;

      if (!startDate || !schedule || !shift) {
        throw new HttpErrors.Forbidden('Invalid payload');
      }
      const listSchedules = await getDatesBetween2Date({
        startDate,
        schedule,
        shift,
      });

      const classResult = await this.classroomRepository.create(classroom);

      const listSchedulesWithClass = listSchedules.map(i => ({
        ...i,
        classroomId: classResult.id,
      }));
      await this.scheduleRepository.createAll(listSchedulesWithClass);

      return classResult;
    } catch (e) {
      throw new HttpErrors.Forbidden(e.message);
    }
  }

  @get('/classrooms/count')
  @response(200, {
    description: 'Classroom model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(
    @param.where(Classroom) where?: Where<Classroom>,
  ): Promise<Count> {
    return this.classroomRepository.count(where);
  }

  @get('/classrooms')
  @response(200, {
    description: 'Array of Classroom model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(Classroom, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.filter(Classroom) filter?: Filter<Classroom>,
  ): Promise<AnyObject[]> {
    const listClassroom = await this.classroomRepository.find(filter);

    const result = await Promise.all(
      listClassroom.map(async i => {
        const schedule = await Promise.all(
          i.schedule.map(async s => {
            if (s.shiftId && s.roomId) {
              const [shift, room] = await Promise.all([
                this.shiftRepository.findById(s.shiftId),
                this.roomRepository.findById(s.roomId),
              ]);
              return {
                ...s,
                shift,
                room,
              };
            }

            return s;
          }),
        );

        const studentCount = (
          await this.userRepository.count({
            classroomId: i.id,
          })
        )?.count;

        return {
          ...i,
          studentCount,
          schedule,
        };
      }),
    );

    return result;
  }

  @patch('/classrooms')
  @response(200, {
    description: 'Classroom PATCH success count',
    content: {'application/json': {schema: CountSchema}},
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Classroom, {partial: true}),
        },
      },
    })
    classroom: Classroom,
    @param.where(Classroom) where?: Where<Classroom>,
  ): Promise<Count> {
    return this.classroomRepository.updateAll(classroom, where);
  }

  @get('/classrooms/{id}')
  @response(200, {
    description: 'Classroom model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(Classroom, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(Classroom, {exclude: 'where'})
    filter?: FilterExcludingWhere<Classroom>,
  ): Promise<Classroom> {
    return this.classroomRepository.findById(id, filter);
  }

  @patch('/classrooms/{id}')
  @response(204, {
    description: 'Classroom PATCH success',
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Classroom, {partial: true}),
        },
      },
    })
    classroom: Classroom,
  ): Promise<void> {
    await this.classroomRepository.updateById(id, classroom);
  }

  @put('/classrooms/{id}')
  @response(204, {
    description: 'Classroom PUT success',
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() classroom: Classroom,
  ): Promise<void> {
    await this.classroomRepository.replaceById(id, classroom);
  }

  @del('/classrooms/{id}')
  @response(204, {
    description: 'Classroom DELETE success',
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    const transaction = await this.classroomRepository.beginTransaction({
      isolationLevel: IsolationLevel.READ_COMMITTED,
      timeout: TRANSACTION_TIMEOUT,
    });

    await this.classroomRepository.deleteById(id, transaction);

    await this.scheduleRepository.deleteAll(
      {
        classroomId: id,
      },
      transaction,
    );
  }
}
