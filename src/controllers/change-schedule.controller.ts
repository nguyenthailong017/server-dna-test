import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  del,
  requestBody,
  response,
} from '@loopback/rest';
import {ChangeSchedule} from '../models';
import {
  ChangeScheduleRepository,
  ScheduleRepository,
  UserRepository,
} from '../repositories';

export class ChangeScheduleController {
  constructor(
    @repository(ChangeScheduleRepository)
    public changeScheduleRepository: ChangeScheduleRepository,
    @repository(UserRepository)
    public userRepository: UserRepository,
    @repository(ScheduleRepository)
    public scheduleRepository: ScheduleRepository,
  ) {}

  @post('/change-schedules')
  @response(200, {
    description: 'ChangeSchedule model instance',
    content: {'application/json': {schema: getModelSchemaRef(ChangeSchedule)}},
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ChangeSchedule, {
            title: 'NewChangeSchedule',
            exclude: ['id'],
          }),
        },
      },
    })
    changeSchedule: Omit<ChangeSchedule, 'id'>,
  ): Promise<ChangeSchedule> {
    return this.changeScheduleRepository.create(changeSchedule);
  }

  @get('/change-schedules/count', {
    responses: {
      '200': {
        description: 'ChangeSchedule model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(
    @param.where(ChangeSchedule) where?: Where<ChangeSchedule>,
  ): Promise<Count> {
    return this.changeScheduleRepository.count(where);
  }

  @get('/change-schedules')
  @response(200, {
    description: 'Array of ChangeSchedule model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(ChangeSchedule, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.filter(ChangeSchedule) filter?: Filter<ChangeSchedule>,
  ): Promise<any[]> {
    const data = await this.changeScheduleRepository.find(filter);

    const moreData = await Promise.all(
      data.map(async item => {
        const obj = item;
        console.log(1, item);

        if (item.userId) {
          obj.owner = await this.userRepository.findById(item.userId);
        }

        if (item.scheduleId) {
          obj.schedule = await this.scheduleRepository.findById(
            item.scheduleId,
          );
        }
        console.log(2, obj);

        return obj;
      }),
    );

    console.log(3, moreData);

    return moreData;
  }

  @get('/change-schedules/{id}')
  @response(200, {
    description: 'ChangeSchedule model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(ChangeSchedule, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(ChangeSchedule, {exclude: 'where'})
    filter?: FilterExcludingWhere<ChangeSchedule>,
  ): Promise<ChangeSchedule> {
    return this.changeScheduleRepository.findById(id, filter);
  }

  @patch('/change-schedules/{id}')
  @response(204, {
    description: 'ChangeSchedule PATCH success',
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ChangeSchedule, {partial: true}),
        },
      },
    })
    changeSchedule: ChangeSchedule,
  ): Promise<void> {
    await this.changeScheduleRepository.updateById(id, changeSchedule);
  }

  @del('/change-schedules/{id}')
  @response(204, {
    description: 'ChangeSchedule DELETE success',
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.changeScheduleRepository.deleteById(id);
  }
}
