import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
  response,
} from '@loopback/rest';
import {Timekeeping} from '../models';
import {TimekeepingRepository} from '../repositories';

export class TimekeepingController {
  constructor(
    @repository(TimekeepingRepository)
    public timekeepingRepository : TimekeepingRepository,
  ) {}

  @post('/timekeepings')
  @response(200, {
    description: 'Timekeeping model instance',
    content: {'application/json': {schema: getModelSchemaRef(Timekeeping)}},
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Timekeeping, {
            title: 'NewTimekeeping',
            exclude: ['id'],
          }),
        },
      },
    })
    timekeeping: Omit<Timekeeping, 'id'>,
  ): Promise<Timekeeping> {
    return this.timekeepingRepository.create(timekeeping);
  }

  @get('/timekeepings/count')
  @response(200, {
    description: 'Timekeeping model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(
    @param.where(Timekeeping) where?: Where<Timekeeping>,
  ): Promise<Count> {
    return this.timekeepingRepository.count(where);
  }

  @get('/timekeepings')
  @response(200, {
    description: 'Array of Timekeeping model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(Timekeeping, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.filter(Timekeeping) filter?: Filter<Timekeeping>,
  ): Promise<Timekeeping[]> {
    return this.timekeepingRepository.find(filter);
  }

  @patch('/timekeepings')
  @response(200, {
    description: 'Timekeeping PATCH success count',
    content: {'application/json': {schema: CountSchema}},
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Timekeeping, {partial: true}),
        },
      },
    })
    timekeeping: Timekeeping,
    @param.where(Timekeeping) where?: Where<Timekeeping>,
  ): Promise<Count> {
    return this.timekeepingRepository.updateAll(timekeeping, where);
  }

  @get('/timekeepings/{id}')
  @response(200, {
    description: 'Timekeeping model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(Timekeeping, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(Timekeeping, {exclude: 'where'}) filter?: FilterExcludingWhere<Timekeeping>
  ): Promise<Timekeeping> {
    return this.timekeepingRepository.findById(id, filter);
  }

  @patch('/timekeepings/{id}')
  @response(204, {
    description: 'Timekeeping PATCH success',
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Timekeeping, {partial: true}),
        },
      },
    })
    timekeeping: Timekeeping,
  ): Promise<void> {
    await this.timekeepingRepository.updateById(id, timekeeping);
  }

  @put('/timekeepings/{id}')
  @response(204, {
    description: 'Timekeeping PUT success',
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() timekeeping: Timekeeping,
  ): Promise<void> {
    await this.timekeepingRepository.replaceById(id, timekeeping);
  }

  @del('/timekeepings/{id}')
  @response(204, {
    description: 'Timekeeping DELETE success',
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.timekeepingRepository.deleteById(id);
  }
}
