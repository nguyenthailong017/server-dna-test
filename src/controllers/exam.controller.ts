import {
  AnyObject,
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
  response,
} from '@loopback/rest';
import {ExamManagement} from '../models';
import {ExamManagementRepository, QuestionRepository} from '../repositories';

export class ExamController {
  constructor(
    @repository(ExamManagementRepository)
    public examManagementRepository: ExamManagementRepository,
    @repository(QuestionRepository)
    public questionRepository: QuestionRepository,
  ) {}

  @post('/exam-managements')
  @response(200, {
    description: 'ExamManagement model instance',
    content: {'application/json': {schema: getModelSchemaRef(ExamManagement)}},
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ExamManagement, {
            title: 'NewExamManagement',
            exclude: ['id'],
          }),
        },
      },
    })
    examManagement: Omit<ExamManagement, 'id'>,
  ): Promise<ExamManagement> {
    return this.examManagementRepository.create(examManagement);
  }

  @get('/exam-managements/count')
  @response(200, {
    description: 'ExamManagement model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(
    @param.where(ExamManagement) where?: Where<ExamManagement>,
  ): Promise<Count> {
    return this.examManagementRepository.count(where);
  }

  @get('/exam-managements')
  @response(200, {
    description: 'Array of ExamManagement model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(ExamManagement, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.filter(ExamManagement) filter?: Filter<ExamManagement>,
  ): Promise<ExamManagement[]> {
    return this.examManagementRepository.find(filter);
  }

  @patch('/exam-managements')
  @response(200, {
    description: 'ExamManagement PATCH success count',
    content: {'application/json': {schema: CountSchema}},
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ExamManagement, {partial: true}),
        },
      },
    })
    examManagement: ExamManagement,
    @param.where(ExamManagement) where?: Where<ExamManagement>,
  ): Promise<Count> {
    return this.examManagementRepository.updateAll(examManagement, where);
  }

  @get('/exam-managements/{id}')
  @response(200, {
    description: 'ExamManagement model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(ExamManagement, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(ExamManagement, {exclude: 'where'})
    filter?: FilterExcludingWhere<ExamManagement>,
  ): Promise<ExamManagement> {
    return this.examManagementRepository.findById(id, filter);
  }

  @patch('/exam-managements/{id}')
  @response(204, {
    description: 'ExamManagement PATCH success',
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ExamManagement, {partial: true}),
        },
      },
    })
    examManagement: ExamManagement,
  ): Promise<void> {
    await this.examManagementRepository.updateById(id, examManagement);
  }

  @put('/exam-managements/{id}')
  @response(204, {
    description: 'ExamManagement PUT success',
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() examManagement: ExamManagement,
  ): Promise<void> {
    await this.examManagementRepository.replaceById(id, examManagement);
  }

  @del('/exam-managements/{id}')
  @response(204, {
    description: 'ExamManagement DELETE success',
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.examManagementRepository.deleteById(id);
  }

  @get('/exam-result')
  @response(200, {
    description: 'Array of exam result model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(ExamManagement, {includeRelations: true}),
        },
      },
    },
  })
  async examResult(
    @param.filter(ExamManagement) filter?: Filter<ExamManagement>,
  ): Promise<AnyObject[]> {
    try {
      const exam = await this.examManagementRepository.findOne({
        where: {
          id: 1,
        },
      });
      const examResult = exam?.examResult && JSON.parse(exam?.examResult || '');
      const questionIds = examResult?.map((item: {id: number}) => item?.id);
      const question = await this.questionRepository.find({
        where: {
          id: {
            inq: questionIds,
          },
        },
        include: [
          {
            relation: 'answerResult',
            scope: {
              fields: ['content', 'questionId'],
            },
          },
        ],
      });

      const getResultById = (id: number) =>
        examResult?.find((item: {id: number}) => item?.id === id);

      const data = question?.map(items => {
        const isCorret =
          items?.answerResult?.content === getResultById(items?.id)?.answer;
        return {
          ...items,
          isCorret,
          answerResult: !isCorret ? items?.answerResult?.content : null,
        };
      });

      return data;
    } catch (error) {
      throw error;
    }
  }
}
