import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  del,
  get,
  getModelSchemaRef,
  getWhereSchemaFor,
  param,
  patch,
  post,
  requestBody,
} from '@loopback/rest';
import {
  Partner,
  MediaContent,
} from '../models';
import {PartnerRepository} from '../repositories';

export class PartnerMediaContentController {
  constructor(
    @repository(PartnerRepository) protected partnerRepository: PartnerRepository,
  ) { }

  @get('/partners/{id}/media-content', {
    responses: {
      '200': {
        description: 'Partner has one MediaContent',
        content: {
          'application/json': {
            schema: getModelSchemaRef(MediaContent),
          },
        },
      },
    },
  })
  async get(
    @param.path.number('id') id: number,
    @param.query.object('filter') filter?: Filter<MediaContent>,
  ): Promise<MediaContent> {
    return this.partnerRepository.mediaContent(id).get(filter);
  }

  @post('/partners/{id}/media-content', {
    responses: {
      '200': {
        description: 'Partner model instance',
        content: {'application/json': {schema: getModelSchemaRef(MediaContent)}},
      },
    },
  })
  async create(
    @param.path.number('id') id: typeof Partner.prototype.id,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(MediaContent, {
            title: 'NewMediaContentInPartner',
            exclude: ['id'],
            optional: ['partnerId']
          }),
        },
      },
    }) mediaContent: Omit<MediaContent, 'id'>,
  ): Promise<MediaContent> {
    return this.partnerRepository.mediaContent(id).create(mediaContent);
  }

  @patch('/partners/{id}/media-content', {
    responses: {
      '200': {
        description: 'Partner.MediaContent PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async patch(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(MediaContent, {partial: true}),
        },
      },
    })
    mediaContent: Partial<MediaContent>,
    @param.query.object('where', getWhereSchemaFor(MediaContent)) where?: Where<MediaContent>,
  ): Promise<Count> {
    return this.partnerRepository.mediaContent(id).patch(mediaContent, where);
  }

  @del('/partners/{id}/media-content', {
    responses: {
      '200': {
        description: 'Partner.MediaContent DELETE success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async delete(
    @param.path.number('id') id: number,
    @param.query.object('where', getWhereSchemaFor(MediaContent)) where?: Where<MediaContent>,
  ): Promise<Count> {
    return this.partnerRepository.mediaContent(id).delete(where);
  }
}
