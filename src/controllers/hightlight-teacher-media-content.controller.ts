import { inject } from '@loopback/core';
import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  del,
  get,
  getModelSchemaRef,
  getWhereSchemaFor,
  param,
  patch,
  post,
  Request,
  requestBody,
  Response,
  RestBindings,
} from '@loopback/rest';
import {
  HightlightTeacher,
  MediaContent,
} from '../models';
import {HightlightTeacherRepository} from '../repositories';
import { FileUploadController } from './file.controller';

export class HightlightTeacherMediaContentController {
  constructor(
    @repository(HightlightTeacherRepository) protected hightlightTeacherRepository: HightlightTeacherRepository,
    @inject('controllers.FileUploadController')
    private fileUploadController: FileUploadController,

    ) { }

  @get('/hightlight-teachers/{id}/media-content', {
    responses: {
      '200': {
        description: 'HightlightTeacher has one MediaContent',
        content: {
          'application/json': {
            schema: getModelSchemaRef(MediaContent),
          },
        },
      },
    },
  })
  async get(
    @param.path.number('id') id: number,
    @param.query.object('filter') filter?: Filter<MediaContent>,
  ): Promise<MediaContent> {
    return this.hightlightTeacherRepository.mediaContent(id).get(filter);
  }

  @post('/hightlight-teachers/{id}/media-content', {
    responses: {
      '200': {
        description: 'HightlightTeacher model instance',
        content: {'application/json': {schema: getModelSchemaRef(MediaContent)}},
      },
    },
  })
  async create(
    @param.path.number('id') id: typeof HightlightTeacher.prototype.id,
    @requestBody.file({
      required: true,
      description: 'Upload file',
    })
    request: Request,
    @inject(RestBindings.Http.RESPONSE) response: Response,
  ): Promise<MediaContent[]> {
    const files: any = await this.fileUploadController.uploadFiles(
      request,
      response,
    );
    const result: MediaContent[] = await Promise.all(
      files.map(async (file: any) => {
        return this.hightlightTeacherRepository.mediaContent(id).create(file);
      }),
    );

    return result;
  }

  @patch('/hightlight-teachers/{id}/media-content', {
    responses: {
      '200': {
        description: 'HightlightTeacher.MediaContent PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async patch(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(MediaContent, {partial: true}),
        },
      },
    })
    mediaContent: Partial<MediaContent>,
    @param.query.object('where', getWhereSchemaFor(MediaContent)) where?: Where<MediaContent>,
  ): Promise<Count> {
    return this.hightlightTeacherRepository.mediaContent(id).patch(mediaContent, where);
  }

  @del('/hightlight-teachers/{id}/media-content', {
    responses: {
      '200': {
        description: 'HightlightTeacher.MediaContent DELETE success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async delete(
    @param.path.number('id') id: number,
    @param.query.object('where', getWhereSchemaFor(MediaContent)) where?: Where<MediaContent>,
  ): Promise<Count> {
    return this.hightlightTeacherRepository.mediaContent(id).delete(where);
  }
}
