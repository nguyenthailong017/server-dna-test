import { inject } from '@loopback/core';
import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  del,
  get,
  getModelSchemaRef,
  getWhereSchemaFor,
  param,
  patch,
  post,
  requestBody,
  RestBindings,
  Response,
  Request,
} from '@loopback/rest';
import {
  CaseStudy,
  MediaContent,
} from '../models';
import {CaseStudyRepository} from '../repositories';
import { FileUploadController } from './file.controller';

export class CaseStudyMediaContentController {
  constructor(
    @repository(CaseStudyRepository) protected caseStudyRepository: CaseStudyRepository,
    @inject('controllers.FileUploadController')
    private fileUploadController: FileUploadController,
  ) { }

  @get('/case-studies/{id}/media-content', {
    responses: {
      '200': {
        description: 'CaseStudy has one MediaContent',
        content: {
          'application/json': {
            schema: getModelSchemaRef(MediaContent),
          },
        },
      },
    },
  })
  async get(
    @param.path.number('id') id: number,
    @param.query.object('filter') filter?: Filter<MediaContent>,
  ): Promise<MediaContent> {
    return this.caseStudyRepository.mediaContent(id).get(filter);
  }

  @post('/case-studies/{id}/media-content', {
    responses: {
      '200': {
        description: 'CaseStudy model instance',
        content: {'application/json': {schema: getModelSchemaRef(MediaContent)}},
      },
    },
  })
  async create(
    @param.path.number('id') id: typeof CaseStudy.prototype.id,
    @requestBody.file({
      required: true,
      description: 'Upload file',
    })
    request: Request,
    @inject(RestBindings.Http.RESPONSE) response: Response,
  ): Promise<MediaContent[]> {
    const files: any = await this.fileUploadController.uploadFiles(
      request,
      response,
    );

    const result: MediaContent[] = await Promise.all(
      files.map(async (file: any) => {
        return this.caseStudyRepository.mediaContent(id).create(file);
      }),
    );

    return result;
  }

  @patch('/case-studies/{id}/media-content', {
    responses: {
      '200': {
        description: 'CaseStudy.MediaContent PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async patch(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(MediaContent, {partial: true}),
        },
      },
    })
    mediaContent: Partial<MediaContent>,
    @param.query.object('where', getWhereSchemaFor(MediaContent)) where?: Where<MediaContent>,
  ): Promise<Count> {
    return this.caseStudyRepository.mediaContent(id).patch(mediaContent, where);
  }

  @del('/case-studies/{id}/media-content', {
    responses: {
      '200': {
        description: 'CaseStudy.MediaContent DELETE success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async delete(
    @param.path.number('id') id: number,
    @param.query.object('where', getWhereSchemaFor(MediaContent)) where?: Where<MediaContent>,
  ): Promise<Count> {
    return this.caseStudyRepository.mediaContent(id).delete(where);
  }
}
