import {inject} from '@loopback/core';
import {Request, HttpErrors} from '@loopback/rest';
import {FILE_UPLOAD_SERVICE, STORAGE_DIRECTORY} from '../keys';
import {FileUploadHandler} from '../types';
import fs from 'fs';
import path from 'path';
import * as AWS from 'aws-sdk';
import uuid from 'uuid-random';
import {rgbToHex, unlinkPromise} from '../utils';
import sharp, {AvailableFormatInfo, FormatEnum} from 'sharp';
import ffmpeg from 'fluent-ffmpeg';
// @ts-ignore
import ColorThief from 'colorthief';

export const CLOUD_S3_STORAGE = {
  S3_BUCKET: process.env.S3_BUCKET ?? '',
  S3_KEY: process.env.S3_KEY ?? '',
  S3_SECRET: process.env.S3_SECRET ?? '',
  S3_ENDPOINT: process.env.S3_ENDPOINT ?? '',
  S3_ENDPOINT_GET_DATA: process.env.S3_ENDPOINT_GET_DATA ?? '',
  S3_FORCE_PATH_STYLE: true,
  S3_SIGNATURE_VERSION: 's3',
  S3_SSL_ENABLE: false,
  S3_HTTP_OPTIONS: {timeout: 1000 * 60000},
};

const s3 = new AWS.S3({
  accessKeyId: CLOUD_S3_STORAGE.S3_KEY,
  secretAccessKey: CLOUD_S3_STORAGE.S3_SECRET,
  // endpoint: CLOUD_S3_STORAGE.S3_ENDPOINT,
  // s3ForcePathStyle: CLOUD_S3_STORAGE.S3_FORCE_PATH_STYLE,
  // signatureVersion: CLOUD_S3_STORAGE.S3_SIGNATURE_VERSION,
  // sslEnabled: CLOUD_S3_STORAGE.S3_SSL_ENABLE,
  // httpOptions: CLOUD_S3_STORAGE.S3_HTTP_OPTIONS,
});

const s3options = {
  partSize: 10 * 1024 * 1024,
  queueSize: 5, // how many concurrent uploads
};

export interface ImageProcessOptions {
  format: keyof FormatEnum | AvailableFormatInfo;
  width: number;
  height: number;
  blurNumber?: number;
}

/**
 * A controller to handle file uploads using multipart/form-data media type
 */
export class FileUploadController {
  /**
   * Constructor
   * @param handler - Inject an express request handler to deal with the request
   * @param storageDirectory
   */
  constructor(
    @inject(FILE_UPLOAD_SERVICE) private handler: FileUploadHandler,
    @inject(STORAGE_DIRECTORY) private storageDirectory: string,
  ) {}

  async createBlurImage(
    original: string,
    target: string,
    options?: ImageProcessOptions,
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      const formatOption = options?.format || 'jpeg';
      const resizeOption = {width: 42};
      const readStream = fs.createReadStream(original);
      const writeStream = fs.createWriteStream(target);
      writeStream
        .on('error', err => reject(err))
        .on('finish', () => resolve(writeStream));
      readStream
        .pipe(sharp().toFormat(formatOption).resize(resizeOption).blur())
        .pipe(writeStream);
    });
  }

  async getMetadata(filePath: string): Promise<any> {
    // const {width = 1, height = 1} = await sharp(filePath).metadata();
    return new Promise((resolve, reject) => {
      ffmpeg.ffprobe(filePath, (err, metadata) => {
        if (err) {
          console.log({err});
          reject(err);
        }

        const {streams} = metadata;
        const {width = 1, height = 1} =
          streams.find(item => item.height && item.width) || streams[0];
        const meta = {ratio: width / height, width, height};

        resolve(meta);
      });
    });
  }

  async screenshot(
    original: string,
    storageFolder: string,
    optionsScreenshot: ffmpeg.ScreenshotsConfig,
  ): Promise<string> {
    let targetPath = '';
    return new Promise((resolve, reject) => {
      ffmpeg(original)
        .on('filenames', function (filenames) {
          targetPath = filenames.join(', ');
        })
        .on('end', function () {
          resolve(targetPath);
        })
        .on('error', (err: any) => {
          reject(err);
        })
        .takeScreenshots({...optionsScreenshot}, storageFolder);
    });
  }

  async uploadFiles(request: Request, response: Response | any) {
    try {
      const res = await new Promise<{files: {buffer: any}[]}>(
        (resolve, reject) => {
          // Save file to local storage before upload to s3 storage
          //@ts-ignore
          return this.handler(request, response, (err: any) => {
            if (err) reject(err);
            else {
              const data = this.getFilesAndFields(request);

              // @ts-ignore
              return resolve({...data});
            }
          });
        },
      );

      const result = await Promise.all(
        res.files.map(async (file: any) => {
          const filename = uuid();

          // Upload file to s3 storage
          const originalImageStream = fs.createReadStream(file.filePath);

          // Get metadata
          const {width, height} = await sharp(file.filePath).metadata();

          // Create blur image
          const blurTargetPath = `${this.storageDirectory}/utopia-blur-${filename}.jpeg`;

          const [color, palette] = await Promise.all([
            ColorThief.getColor(file.filePath),
            ColorThief.getPalette(file.filePath, 5),
            this.createBlurImage(file.filePath, blurTargetPath),
          ]);

          const blurImageStream = fs.createReadStream(blurTargetPath);

          const params = {
            ContentType: file.type || 'image/jpeg',
            ACL: 'public-read',
            Bucket: CLOUD_S3_STORAGE.S3_BUCKET + '/3',
          };

          const [originalUploadResult, blurUploadResult] = await Promise.all([
            s3
              .upload(
                {
                  ...params,
                  Key: `utopia-mt${filename}.jpeg`,
                  Body: originalImageStream,
                },
                s3options,
              )
              .promise(),
            s3
              .upload(
                {
                  ...params,
                  Key: `utopia-blur-mt${filename}.jpeg`,
                  Body: blurImageStream,
                },
                s3options,
              )
              .promise(),
          ]);

          // Remove file in local storage after upload success
          unlinkPromise(file.filePath);
          unlinkPromise(blurTargetPath);

          return {
            size: file.size,
            filePath: originalUploadResult.Bucket,
            key: originalUploadResult.Key,
            blurKey: blurUploadResult.Key,
            url: `${CLOUD_S3_STORAGE.S3_ENDPOINT_GET_DATA}/${originalUploadResult.Key}`,
            urlBlur: `${CLOUD_S3_STORAGE.S3_ENDPOINT_GET_DATA}/${blurUploadResult.Key}`,
            width,
            height,
            dominantColor: rgbToHex(color),
            palette: rgbToHex(palette, true),
          };
        }),
      );

      return result;
    } catch (e) {
      return e;
    }
  }

  async uploadVideo(request: Request, response: Response | any) {
    try {
      const res = await new Promise<{files: {buffer: any}[]}>(
        (resolve, reject) => {
          // Save file to local storage before upload to s3 storage
          //@ts-ignore
          return this.handler(request, response, (err: any) => {
            if (err) reject(err);
            else {
              const data = this.getFilesAndFields(request);

              // @ts-ignore
              return resolve({...data});
            }
          });
        },
      );

      const result = await Promise.all(
        res.files.map(async (file: any) => {
          const filename = uuid();

          const {width, height, ratio} = await this.getMetadata(file.filePath);

          // Create poster - thumnail for video

          const screenshot = await this.screenshot(
            file.filePath,
            path.join(__dirname, '../../.sandbox'),
            {
              filename,
              count: 1,
              timemarks: ['00:00:01.000'],
              size: `480x${(480 / ratio).toFixed(0)}`,
            },
          );

          // Create blur image
          const screenshotPath = `${this.storageDirectory}/${screenshot}`;
          const blurScreenshotPath = `${this.storageDirectory}/${filename}.jpeg`;

          await this.createBlurImage(screenshotPath, blurScreenshotPath);

          const screenshotStream = fs.createReadStream(screenshotPath);
          const blurImageStream = fs.createReadStream(blurScreenshotPath);

          const params = {
            ContentType: file.type,
            ACL: 'public-read',
            Bucket: CLOUD_S3_STORAGE.S3_BUCKET + '/3',
          };

          // Upload file to s3 storage
          const originalVideoStream = fs.createReadStream(file.filePath);

          const [originalUpload, screenUpload, blurScreenUpload] =
            await Promise.all([
              s3
                .upload(
                  {
                    ...params,
                    Key: `utopia-video-mt${filename}`,
                    Body: originalVideoStream,
                  },
                  s3options,
                )
                .promise(),
              s3
                .upload(
                  {
                    ...params,
                    Key: `utopia-screen-mt${filename}`,
                    Body: screenshotStream,
                  },
                  s3options,
                )
                .promise(),
              s3
                .upload(
                  {
                    ...params,
                    Key: `utopia-blur-mt${filename}`,
                    Body: blurImageStream,
                  },
                  s3options,
                )
                .promise(),
            ]);

          // Remove file in local storage after upload success
          unlinkPromise(file.filePath);
          unlinkPromise(screenshotPath);
          unlinkPromise(blurScreenshotPath);

          return {
            size: file.size,
            filePath: originalUpload.Bucket,
            key: screenUpload.Key,
            blurKey: blurScreenUpload.Key,
            videoKey: originalUpload.Key,
            url: `${CLOUD_S3_STORAGE.S3_ENDPOINT_GET_DATA}/${screenUpload.Bucket}/${screenUpload.Key}`,
            urlBlur: `${CLOUD_S3_STORAGE.S3_ENDPOINT_GET_DATA}/${blurScreenUpload.Bucket}/${blurScreenUpload.Key}`,
            videoUrl: `${CLOUD_S3_STORAGE.S3_ENDPOINT_GET_DATA}/${originalUpload.Bucket}/${originalUpload.Key}`,
            width,
            height,
          };
        }),
      );

      return result;
    } catch (e) {
      return e;
    }
  }

  getFilesAndFields(request: Request) {
    // @ts-ignore
    const uploadedFiles = request.files;
    // @ts-ignore
    const mapper = (f: globalThis.Express.Multer.File) => {
      const filePath = path.resolve(this.storageDirectory, f.filename);

      return {
        filename: f.filename,
        originalname: f.originalname,
        encoding: f.encoding,
        mimetype: f.mimetype,
        type: f.mimetype,
        size: f.size,
        filePath,
      };
    };
    let files: object[] = [];
    if (Array.isArray(uploadedFiles)) {
      files = uploadedFiles.map(mapper);
    } else {
      for (const filename in uploadedFiles) {
        files.push(...uploadedFiles[filename].map(mapper));
      }
    }
    // @ts-ignore
    return {files, fields: request.body};
  }

  validateFileName(fileName: string) {
    const resolved = path.resolve(this.storageDirectory, fileName);
    if (resolved.startsWith(this.storageDirectory)) return resolved;
    // The resolved file is outside sandbox
    throw new HttpErrors.BadRequest(`Invalid file name: ${fileName}`);
  }
}
