import {authenticate} from '@loopback/authentication';
import {OPERATION_SECURITY_SPEC} from '@loopback/authentication-jwt';
import {authorize} from '@loopback/authorization';
import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  del,
  requestBody,
  response,
} from '@loopback/rest';
import {AUTHORIZE_ADMIN} from '../constants';
import {Courses} from '../models';
import {CoursesRepository} from '../repositories';

export class CoursesController {
  constructor(
    @repository(CoursesRepository)
    public coursesRepository: CoursesRepository,
  ) {}

  @post('/courses', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'Courses model instance',
        content: {'application/json': {schema: getModelSchemaRef(Courses)}},
      },
    },
  })
  @authenticate('jwt')
  @authorize(AUTHORIZE_ADMIN)
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Courses, {
            title: 'NewCourses',
            exclude: ['id', 'creatorId', 'mediaContents', 'createdAt', 'updatedAt'],
          }),
        },
      },
    })
    courses: Omit<Courses, 'id'>,
  ): Promise<Courses> {
    return this.coursesRepository.create(courses);
  }

  @get('/courses/count')
  @response(200, {
    description: 'Courses model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(@param.where(Courses) where?: Where<Courses>): Promise<Count> {
    return this.coursesRepository.count(where);
  }

  @get('/courses')
  @response(200, {
    description: 'Array of Courses model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(Courses, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.filter(Courses) filter?: Filter<Courses>,
  ): Promise<{data: Courses[], total: number}> {
    try {
      const [data, total] = await Promise.all([
        this.coursesRepository.find(filter),
        this.coursesRepository.count(filter?.where).then(res => res?.count),
      ]);
      return {
        data,
        total,
      };
    } catch (error) {
      return {
        data: [],
        total: 0,
      };
    }
  }

  @get('/courses/{id}')
  @response(200, {
    description: 'Courses model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(Courses, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(Courses, {exclude: 'where'})
    filter?: FilterExcludingWhere<Courses>,
  ): Promise<Courses> {
    return this.coursesRepository.findById(id, filter);
  }

  @patch('/courses/{id}', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '204': {
        description: 'Courses PATCH success',
        content: {'application/json': {schema: getModelSchemaRef(Courses)}},
      },
    },
  })
  @authenticate('jwt')
  @authorize(AUTHORIZE_ADMIN)
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Courses, {
            partial: true,
            title: 'NewCourses',
            exclude: ['id', 'creatorId', 'mediaContents', 'createdAt', 'updatedAt'],
          }),
        },
      },
    })
    courses: Courses,
  ): Promise<Courses> {
    await this.coursesRepository.updateById(id, courses);
    return this.coursesRepository.findById(id);
  }

  @del('/courses/{id}', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '204': {
        description: 'Courses DELETE success',
      },
    },
  })
  @authenticate('jwt')
  @authorize(AUTHORIZE_ADMIN)
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.coursesRepository.deleteById(id);
  }
}
