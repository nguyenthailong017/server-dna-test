import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
  response,
} from '@loopback/rest';
import {StaffSchedule} from '../models';
import {StaffScheduleRepository} from '../repositories';

export class StaffScheduleController {
  constructor(
    @repository(StaffScheduleRepository)
    public staffScheduleRepository : StaffScheduleRepository,
  ) {}

  @post('/staff-schedules')
  @response(200, {
    description: 'StaffSchedule model instance',
    content: {'application/json': {schema: getModelSchemaRef(StaffSchedule)}},
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(StaffSchedule, {
            title: 'NewStaffSchedule',
            exclude: ['id'],
          }),
        },
      },
    })
    staffSchedule: Omit<StaffSchedule, 'id'>,
  ): Promise<StaffSchedule> {
    return this.staffScheduleRepository.create(staffSchedule);
  }

  @get('/staff-schedules/count')
  @response(200, {
    description: 'StaffSchedule model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(
    @param.where(StaffSchedule) where?: Where<StaffSchedule>,
  ): Promise<Count> {
    return this.staffScheduleRepository.count(where);
  }

  @get('/staff-schedules')
  @response(200, {
    description: 'Array of StaffSchedule model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(StaffSchedule, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.filter(StaffSchedule) filter?: Filter<StaffSchedule>,
  ): Promise<StaffSchedule[]> {
    return this.staffScheduleRepository.find(filter);
  }

  @patch('/staff-schedules')
  @response(200, {
    description: 'StaffSchedule PATCH success count',
    content: {'application/json': {schema: CountSchema}},
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(StaffSchedule, {partial: true}),
        },
      },
    })
    staffSchedule: StaffSchedule,
    @param.where(StaffSchedule) where?: Where<StaffSchedule>,
  ): Promise<Count> {
    return this.staffScheduleRepository.updateAll(staffSchedule, where);
  }

  @get('/staff-schedules/{id}')
  @response(200, {
    description: 'StaffSchedule model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(StaffSchedule, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(StaffSchedule, {exclude: 'where'}) filter?: FilterExcludingWhere<StaffSchedule>
  ): Promise<StaffSchedule> {
    return this.staffScheduleRepository.findById(id, filter);
  }

  @patch('/staff-schedules/{id}')
  @response(204, {
    description: 'StaffSchedule PATCH success',
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(StaffSchedule, {partial: true}),
        },
      },
    })
    staffSchedule: StaffSchedule,
  ): Promise<void> {
    await this.staffScheduleRepository.updateById(id, staffSchedule);
  }

  @put('/staff-schedules/{id}')
  @response(204, {
    description: 'StaffSchedule PUT success',
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() staffSchedule: StaffSchedule,
  ): Promise<void> {
    await this.staffScheduleRepository.replaceById(id, staffSchedule);
  }

  @del('/staff-schedules/{id}')
  @response(204, {
    description: 'StaffSchedule DELETE success',
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.staffScheduleRepository.deleteById(id);
  }
}
