import {UserRepository} from '@loopback/authentication-jwt';
import {
  Filter,
  FilterExcludingWhere,
  PredicateComparison,
  repository,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  del,
  requestBody,
  response,
} from '@loopback/rest';
import {Schedule} from '../models';
import {ClassroomRepository, ScheduleRepository} from '../repositories';

export class ScheduleController {
  constructor(
    @repository(ScheduleRepository)
    public scheduleRepository: ScheduleRepository,
    @repository(ClassroomRepository)
    public classroomRepository: ClassroomRepository,
    @repository(UserRepository)
    public userRepository: UserRepository,
  ) {}

  @post('/schedules')
  @response(200, {
    description: 'Schedule model instance',
    content: {'application/json': {schema: getModelSchemaRef(Schedule)}},
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Schedule, {
            title: 'NewSchedule',
            exclude: ['id'],
          }),
        },
      },
    })
    schedule: Omit<Schedule, 'id'>,
  ): Promise<Schedule> {
    return this.scheduleRepository.create(schedule);
  }

  @get('/schedules')
  @response(200, {
    description: 'Array of Schedule model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(Schedule, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.filter(Schedule) filter?: Filter<Schedule>,
  ): Promise<Schedule[]> {
    const res = await this.scheduleRepository.find(filter);

    return res;
  }

  @get('/schedules/student/{studentId}')
  @response(200, {
    description: 'Array of Schedule model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(Schedule, {includeRelations: true}),
        },
      },
    },
  })
  async findScheduleByStudent(
    @param.path.number('studentId') studentId: number,
    @param.filter(Schedule) filter?: Filter<Schedule>,
  ): Promise<Schedule[]> {
    let listClassroomIds: any = [];
    if (studentId) {
      listClassroomIds = (
        await this.userRepository.find({
          where: {id: studentId},
          fields: ['id', 'classroomId'],
        })
      )?.map(i => i.classroomId);

      console.log({listClassroomIds});
    }
    return this.scheduleRepository.find({
      ...filter,
      where: {
        ...filter?.where,
        classroomId: {
          inq: listClassroomIds,
        },
      },
    });
  }

  @get('/schedules/teacher/{teacherId}')
  @response(200, {
    description: 'Array of Schedule model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(Schedule, {includeRelations: true}),
        },
      },
    },
  })
  async findScheduleByTeacher(
    @param.path.number('teacherId') teacherId: number,
    @param.filter(Schedule) filter?: Filter<Schedule>,
  ): Promise<Schedule[]> {
    let listClassroomIds: any = [];
    if (teacherId) {
      listClassroomIds = (
        await this.classroomRepository.find({
          where: {teacherIds: teacherId as PredicateComparison<number[]>},
          fields: ['id', 'teacherIds'],
        })
      )?.map(i => i.id);

      console.log({listClassroomIds});
    }
    return this.scheduleRepository.find({
      ...filter,
      where: {
        ...filter?.where,
        classroomId: {
          inq: listClassroomIds,
        },
      },
    });
  }

  @get('/schedules/{id}')
  @response(200, {
    description: 'Schedule model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(Schedule, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(Schedule, {exclude: 'where'})
    filter?: FilterExcludingWhere<Schedule>,
  ): Promise<Schedule> {
    return this.scheduleRepository.findById(id, filter);
  }

  @patch('/schedules/{id}')
  @response(204, {
    description: 'Schedule PATCH success',
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Schedule, {partial: true}),
        },
      },
    })
    schedule: Schedule,
  ): Promise<void> {
    await this.scheduleRepository.updateById(id, schedule);
  }

  @del('/schedules/{id}')
  @response(204, {
    description: 'Schedule DELETE success',
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.scheduleRepository.deleteById(id);
  }
}
