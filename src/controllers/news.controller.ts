import {
  AnyObject,
  Filter,
  FilterExcludingWhere,
  repository,
} from '@loopback/repository';
import {
  del,
  get,
  getModelSchemaRef,
  param,
  patch,
  post,
  requestBody,
} from '@loopback/rest';
import {Courses, News, NewsRelations, Review, User} from '../models';
import {
  CoursesRepository,
  HightlightTeacherRepository,
  MediaContentRepository,
  NewsRepository,
  ReviewRepository,
} from '../repositories';
import {authenticate} from '@loopback/authentication';
import {authorize} from '@loopback/authorization';
import {OPERATION_SECURITY_SPEC} from '../utils';
import {AUTHORIZE_ALL} from '../constants';
import {ORDER} from '../constants/order';

export class NewsController {
  constructor(
    @repository(NewsRepository)
    public newsRepository: NewsRepository,
    @repository(CoursesRepository)
    public coursesRepository: CoursesRepository,
    @repository(HightlightTeacherRepository)
    public hightlightTeacherRepository: HightlightTeacherRepository,
    @repository(ReviewRepository)
    public reviewRepository: ReviewRepository,
    @repository(MediaContentRepository)
    public mediaContentRepository: MediaContentRepository,
  ) {}

  @post('/news', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'News model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(News),
          },
        },
      },
    },
  })
  @authenticate('jwt')
  @authorize(AUTHORIZE_ALL)
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(News, {
            title: 'NewNews',
            exclude: ['id', 'createdAt', 'updatedAt', 'slug', 'creatorId'],
          }),
        },
      },
    })
    news: Omit<News, 'id'>,
  ): Promise<News> {
    return this.newsRepository.create(news);
  }

  @get('/news', {
    responses: {
      '200': {
        description: 'Array of News model instances',
        content: {
          'application/json': {
            schema: {
              title: 'listBookings',
              type: 'object',
              properties: {
                total: {
                  type: 'number',
                },
                data: {
                  type: 'array',
                  items: getModelSchemaRef(News, {includeRelations: true}),
                },
              },
            },
          },
        },
      },
    },
  })
  async find(
    @param.filter(News) filter?: Filter<News>,
  ): Promise<{data: News[]; total: number}> {
    const [data, count] = await Promise.all([
      this.newsRepository.find(filter),
      this.newsRepository.count(filter && filter.where),
    ]);

    return {
      data,
      total: count.count || 0,
    };
  }

  @get('/news/detail/{slug}', {
    responses: {
      '200': {
        description: 'News model instances',
        content: {
          'application/json': {
            schema: {
              title: 'newsBySlug',
              type: 'object',
              properties: {
                total: {
                  type: 'number',
                },
                data: {
                  type: 'array',
                  items: getModelSchemaRef(News, {includeRelations: true}),
                },
              },
            },
          },
        },
      },
    },
  })
  async findBySlug(
    @param.path.string('slug') slug: string,
    @param.filter(News, {exclude: 'where'}) filter?: FilterExcludingWhere<News>,
  ): Promise<(News & NewsRelations) | null> {
    return this.newsRepository.findOne({
      ...filter,
      where: {
        slug,
      },
    });
  }

  @get('/news/{id}', {
    responses: {
      '200': {
        description: 'Array of News model instances',
        content: {
          description: 'News model instance',
          content: {
            'application/json': {
              schema: getModelSchemaRef(News, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(News, {exclude: 'where'}) filter?: FilterExcludingWhere<News>,
  ): Promise<News> {
    return this.newsRepository.findById(id, {
      ...filter,
      include: [
        {
          relation: 'mediaContents',
        },
        {
          relation: 'creator',
        },
      ],
    });
  }

  @patch('/news/{id}', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '204': {
        description: 'News PATCH success',
      },
    },
  })
  @authenticate('jwt')
  @authorize(AUTHORIZE_ALL)
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: {
            type: 'object',
            title: 'UpdateNews',
            properties: {
              title: {
                type: 'string',
              },
              description: {
                type: 'string',
              },
              content: {
                type: 'string',
              },
              tag: {
                type: 'string',
              },
              type: {
                type: 'string',
              },
              status: {
                type: 'string',
              },
            },
          },
        },
      },
    })
    news: News,
  ): Promise<News> {
    await this.newsRepository.updateById(id, news);

    return this.newsRepository.findById(id);
  }

  @del('/news/{id}', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '204': {
        description: 'News DELETE success',
      },
    },
  })
  @authenticate('jwt')
  @authorize(AUTHORIZE_ALL)
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.newsRepository.deleteById(id);
  }

  @get('/home', {
    responses: {
      '200': {
        description: 'Array of Home model instances',
        content: {
          'application/json': {
            schema: {
              title: 'HomeList',
              type: 'object',
              properties: {
                news: {
                  type: 'array',
                  items: getModelSchemaRef(News, {includeRelations: true}),
                },
                courses: {
                  type: 'array',
                  items: getModelSchemaRef(Courses, {includeRelations: true}),
                },
                teachers: {
                  type: 'array',
                  items: getModelSchemaRef(User, {includeRelations: true}),
                },
                feedbacks: {
                  type: 'array',
                  items: getModelSchemaRef(Review, {includeRelations: true}),
                },
              },
            },
          },
        },
      },
    },
  })
  async getHome(): Promise<{
    news: News[];
    courses: Courses[];
    teachers: AnyObject[];
    feedbacks: Review[];
  }> {
    try {
      const [news, courses, teachers, feedbacks] = await Promise.all([
        this.newsRepository.find({
          limit: 3,
          order: ORDER.PRIORITY_DESC,
          include: [
            {
              relation: 'mediaContents',
              scope: {
                limit: 1,
                order: ['id DESC'],
              },
            },
          ],
        }),
        this.coursesRepository.find({
          limit: 5,
          order: ORDER.PRIORITY_DESC,
          include: [
            {
              relation: 'mediaContents',
              scope: {
                limit: 1,
                order: ['id DESC'],
              },
            },
          ],
        }),
        this.hightlightTeacherRepository.find({
          limit: 3,
          order: ORDER.PRIORITY_DESC,
          include: [
            {
              relation: 'mediaContent',
            },
          ],
        }),
        this.reviewRepository.find({
          limit: 6,
          order: ORDER.LATEST,
          include: [
            {
              relation: 'mediaContent',
            },
          ],
        }),
      ]);

      return {
        news,
        courses,
        teachers,
        feedbacks,
      };
    } catch (error) {
      return {
        news: [],
        courses: [],
        teachers: [],
        feedbacks: [],
      };
    }
  }
}
