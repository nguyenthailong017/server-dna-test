import {UserService} from '@loopback/authentication';
import {inject} from '@loopback/core';
import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  del,
  requestBody,
  response,
} from '@loopback/rest';
import {pick} from 'ramda';
import {PasswordHasherBindings, UserServiceBindings} from '../keys';
import {Teacher, User} from '../models';
import {Credentials, TeacherRepository, UserRepository} from '../repositories';
import {
  PasswordHasher,
  UserManagementService,
  validateCredentials,
} from '../services';
import {NewUserRequest} from './user-management.controller';

export class TeacherController {
  constructor(
    @repository(TeacherRepository)
    public teacherRepository: TeacherRepository,
    @inject(UserServiceBindings.USER_SERVICE)
    public userService: UserService<User, Credentials>,
    @inject(UserServiceBindings.USER_SERVICE)
    public userManagementService: UserManagementService,
    @repository(UserRepository)
    public userRepository: UserRepository,
    @inject(PasswordHasherBindings.PASSWORD_HASHER)
    public passwordHasher: PasswordHasher,
  ) {}

  @post('/teachers')
  @response(200, {
    description: 'Teacher model instance',
    content: {'application/json': {schema: getModelSchemaRef(Teacher)}},
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: {
            title: 'NewPost',
            type: 'object',
            properties: {
              teacher: {
                type: 'object',
                properties: {
                  ...getModelSchemaRef(Teacher, {
                    title: 'NewTeacher',
                    exclude: ['id'],
                  }).definitions['NewTeacher'].properties,
                },
              },
              user: {
                type: 'object',
                properties: {
                  ...getModelSchemaRef(NewUserRequest, {
                    title: 'NewUser',
                    exclude: ['id'],
                  }).definitions['NewUser'].properties,
                },
              },
            },
          },
        },
      },
    })
    teacher: {
      user: NewUserRequest;
      teacher: Teacher;
    },
  ): Promise<{user: User; teacher: Teacher}> {
    try {
      validateCredentials(pick(['email', 'password'], teacher.user));
      const resUser = await this.userManagementService.createUser(teacher.user);
      const resTeacher = await this.teacherRepository.create({
        ...teacher.teacher,
        userId: resUser?.id,
      });

      return {
        user: resUser,
        teacher: resTeacher,
      };
    } catch (e) {
      throw e;
    }
  }

  @patch('/teachers/{id}')
  @response(204, {
    description: 'Teacher PATCH success',
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: {
            title: 'NewPost',
            type: 'object',
            properties: {
              teacher: {
                type: 'object',
                properties: {
                  ...getModelSchemaRef(Teacher, {
                    title: 'NewTeacher',
                    partial: true,
                  }).definitions['NewTeacher'].properties,
                },
              },
              user: {
                type: 'object',
                properties: {
                  ...getModelSchemaRef(NewUserRequest, {
                    title: 'NewUser',
                    partial: true,
                  }).definitions['NewUser'].properties,
                },
              },
            },
          },
        },
      },
    })
    teacher: {
      user: NewUserRequest;
      teacher: Teacher;
    },
  ): Promise<{user: User; teacher: Teacher}> {
    try {
      const resTeacher = await this.teacherRepository.updateById(id, {
        ...teacher.teacher,
      });

      const resUser = await this.userRepository.updateById(
        resTeacher?.userId,
        teacher.user,
      );

      return {
        user: resUser,
        teacher: resTeacher,
      };
    } catch (e) {
      throw e;
    }
  }

  @get('/teachers/count')
  @response(200, {
    description: 'Teacher model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(@param.where(Teacher) where?: Where<Teacher>): Promise<Count> {
    return this.teacherRepository.count(where);
  }

  @get('/teachers')
  @response(200, {
    description: 'Array of Teacher model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(Teacher, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.filter(Teacher) filter?: Filter<Teacher>,
  ): Promise<Teacher[]> {
    return this.teacherRepository.find(filter);
  }

  @get('/teachers/{id}')
  @response(200, {
    description: 'Teacher model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(Teacher, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(Teacher, {exclude: 'where'})
    filter?: FilterExcludingWhere<Teacher>,
  ): Promise<Teacher> {
    console.log(1, filter);
    return this.teacherRepository.findById(id, filter);
  }

  @del('/teachers/{id}')
  @response(204, {
    description: 'Teacher DELETE success',
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    const user = await this.teacherRepository.findById(id, {
      fields: ['id', 'userId'],
    });

    await this.teacherRepository.deleteById(id);
    await this.userRepository.deleteById(user.userId);
  }
}
