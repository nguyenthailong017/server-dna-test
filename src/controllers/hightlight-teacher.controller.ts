import {authenticate} from '@loopback/authentication';
import {OPERATION_SECURITY_SPEC} from '@loopback/authentication-jwt';
import {authorize} from '@loopback/authorization';
import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  del,
  requestBody,
  response,
} from '@loopback/rest';
import {AUTHORIZE_ADMIN} from '../constants';
import {HightlightTeacher} from '../models';
import {HightlightTeacherRepository} from '../repositories';

export class HightlishtTeacherControllerController {
  constructor(
    @repository(HightlightTeacherRepository)
    public hightlightTeacherRepository: HightlightTeacherRepository,
  ) {}

  @post('/hightlight-teachers', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'HightlightTeacher model instance',
        content: {
          'application/json': {schema: getModelSchemaRef(HightlightTeacher)},
        },
      },
    },
  })
  @authenticate('jwt')
  @authorize(AUTHORIZE_ADMIN)
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(HightlightTeacher, {
            title: 'NewHightlightTeacher',
            exclude: ['id', 'createdAt', 'updatedAt'],
          }),
        },
      },
    })
    hightlightTeacher: Omit<HightlightTeacher, 'id'>,
  ): Promise<HightlightTeacher> {
    return this.hightlightTeacherRepository.create(hightlightTeacher);
  }

  @get('/hightlight-teachers/count')
  @response(200, {
    description: 'HightlightTeacher model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(
    @param.where(HightlightTeacher) where?: Where<HightlightTeacher>,
  ): Promise<Count> {
    return this.hightlightTeacherRepository.count(where);
  }

  @get('/hightlight-teachers')
  @response(200, {
    description: 'Array of HightlightTeacher model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(HightlightTeacher, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.filter(HightlightTeacher) filter?: Filter<HightlightTeacher>,
  ): Promise<{data: HightlightTeacher[]; total: number}> {
    try {
      const [data, total] = await Promise.all([
        this.hightlightTeacherRepository.find(filter),
        this.hightlightTeacherRepository
          .count(filter?.where)
          .then(res => res?.count),
      ]);
      return {
        data,
        total,
      };
    } catch (e) {
      return {
        data: [],
        total: 0,
      };
    }
  }

  @get('/hightlight-teachers/{id}')
  @response(200, {
    description: 'HightlightTeacher model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(HightlightTeacher, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(HightlightTeacher, {exclude: 'where'})
    filter?: FilterExcludingWhere<HightlightTeacher>,
  ): Promise<HightlightTeacher> {
    return this.hightlightTeacherRepository.findById(id, filter);
  }

  @patch('/hightlight-teachers/{id}', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '204': {
        description: 'HightlightTeacher PATCH success',
        content: {
          'application/json': {schema: getModelSchemaRef(HightlightTeacher)},
        },
      },
    },
  })
  @authenticate('jwt')
  @authorize(AUTHORIZE_ADMIN)
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(HightlightTeacher, {
            partial: true,
            title: 'NewHightlightTeacher',
            exclude: ['id', 'createdAt', 'updatedAt'],
          }),
        },
      },
    })
    hightlightTeacher: Omit<HightlightTeacher, 'id'>,
  ): Promise<HightlightTeacher> {
    await this.hightlightTeacherRepository.updateById(id, hightlightTeacher);
    return this.hightlightTeacherRepository.findById(id);
  }

  @del('/hightlight-teachers/{id}', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '204': {
        description: 'HightlightTeacher DELETE success',
      },
    },
  })
  @authenticate('jwt')
  @authorize(AUTHORIZE_ADMIN)
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.hightlightTeacherRepository.deleteById(id);
  }
}
