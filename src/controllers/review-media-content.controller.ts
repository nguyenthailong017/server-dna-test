import {inject} from '@loopback/core';
import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  del,
  get,
  getModelSchemaRef,
  getWhereSchemaFor,
  param,
  patch,
  post,
  requestBody,
  RestBindings,
  Response,
  Request,
} from '@loopback/rest';
import {Review, MediaContent} from '../models';
import {ReviewRepository} from '../repositories';
import {FileUploadController} from './file.controller';

export class ReviewMediaContentController {
  constructor(
    @repository(ReviewRepository) protected reviewRepository: ReviewRepository,
    @inject('controllers.FileUploadController')
    private fileUploadController: FileUploadController,
  ) {}

  @get('/reviews/{id}/media-content', {
    responses: {
      '200': {
        description: 'Review has one MediaContent',
        content: {
          'application/json': {
            schema: getModelSchemaRef(MediaContent),
          },
        },
      },
    },
  })
  async get(
    @param.path.number('id') id: number,
    @param.query.object('filter') filter?: Filter<MediaContent>,
  ): Promise<MediaContent> {
    return this.reviewRepository.mediaContent(id).get(filter);
  }

  @post('/reviews/{id}/media-content', {
    responses: {
      '200': {
        description: 'Review model instance',
        content: {
          'application/json': {schema: getModelSchemaRef(MediaContent)},
        },
      },
    },
  })
  async create(
    @param.path.number('id') id: typeof Review.prototype.id,
    @requestBody.file({
      required: true,
      description: 'Upload file',
    })
    request: Request,
    @inject(RestBindings.Http.RESPONSE) response: Response,
  ): Promise<MediaContent[]> {
    const files: any = await this.fileUploadController.uploadFiles(
      request,
      response,
    );

    const result: MediaContent[] = await Promise.all(
      files.map(async (file: any) => {
        return this.reviewRepository.mediaContent(id).create(file);
      }),
    );

    return result;
  }

  @patch('/reviews/{id}/media-content', {
    responses: {
      '200': {
        description: 'Review.MediaContent PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async patch(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(MediaContent, {partial: true}),
        },
      },
    })
    mediaContent: Partial<MediaContent>,
    @param.query.object('where', getWhereSchemaFor(MediaContent))
    where?: Where<MediaContent>,
  ): Promise<Count> {
    return this.reviewRepository.mediaContent(id).patch(mediaContent, where);
  }

  @del('/reviews/{id}/media-content', {
    responses: {
      '200': {
        description: 'Review.MediaContent DELETE success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async delete(
    @param.path.number('id') id: number,
    @param.query.object('where', getWhereSchemaFor(MediaContent))
    where?: Where<MediaContent>,
  ): Promise<Count> {
    return this.reviewRepository.mediaContent(id).delete(where);
  }
}
