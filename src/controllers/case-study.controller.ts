import {authenticate} from '@loopback/authentication';
import {OPERATION_SECURITY_SPEC} from '@loopback/authentication-jwt';
import {authorize} from '@loopback/authorization';
import {inject} from '@loopback/core';
import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  del,
  requestBody,
  response,
} from '@loopback/rest';
import {AUTHORIZE_ADMIN} from '../constants';
import {CaseStudy} from '../models';
import {CaseStudyRepository} from '../repositories';
import {SecurityBindings, securityId, UserProfile} from '@loopback/security';

export class CaseStudyController {
  constructor(
    @repository(CaseStudyRepository)
    public caseStudyRepository: CaseStudyRepository,
  ) {}

  @post('/case-studies', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'CaseStudy model instance',
        content: {'application/json': {schema: getModelSchemaRef(CaseStudy)}},
      },
    },
  })
  @authenticate('jwt')
  @authorize(AUTHORIZE_ADMIN)
  async create(
    @inject(SecurityBindings.USER)
    currentUserProfile: UserProfile,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(CaseStudy, {
            title: 'NewCaseStudy',
            exclude: ['id', 'createdAt', 'updatedAt', 'slug', 'creatorId'],
          }),
        },
      },
    })
    caseStudy: Omit<CaseStudy, 'id'>,
  ): Promise<CaseStudy> {
    const creatorId: number = Number(currentUserProfile[securityId]);
    const data = {
      ...caseStudy,
      creatorId,
    };
    return this.caseStudyRepository.create(data);
  }

  @get('/case-studies/count')
  @response(200, {
    description: 'CaseStudy model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(
    @param.where(CaseStudy) where?: Where<CaseStudy>,
  ): Promise<Count> {
    return this.caseStudyRepository.count(where);
  }

  @get('/case-studies')
  @response(200, {
    description: 'Array of CaseStudy model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(CaseStudy, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.filter(CaseStudy) filter?: Filter<CaseStudy>,
  ): Promise<{data: CaseStudy[]; total: number}> {
    try {
      const [data, total] = await Promise.all([
        this.caseStudyRepository.find(filter),
        this.caseStudyRepository.count(filter?.where).then(res => res?.count),
      ]);
      return {
        data,
        total,
      };
    } catch (error) {
      return {
        data: [],
        total: 0,
      };
    }
  }

  @get('/case-studies/{id}')
  @response(200, {
    description: 'CaseStudy model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(CaseStudy, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(CaseStudy, {exclude: 'where'})
    filter?: FilterExcludingWhere<CaseStudy>,
  ): Promise<CaseStudy> {
    return this.caseStudyRepository.findById(id, filter);
  }

  @patch('/case-studies/{id}', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'CaseStudy PATCH success',
        content: {'application/json': {schema: getModelSchemaRef(CaseStudy)}},
      },
    },
  })
  @authenticate('jwt')
  @authorize(AUTHORIZE_ADMIN)
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(CaseStudy, {
            partial: true,
            title: 'UpdateCaseStudy',
            exclude: ['id', 'createdAt', 'updatedAt', 'slug', 'creatorId'],
          }),
        },
      },
    })
    caseStudy: CaseStudy,
  ): Promise<CaseStudy> {
    await this.caseStudyRepository.updateById(id, caseStudy);
    return this.caseStudyRepository.findById(id);
  }

  @del('/case-studies/{id}', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '204': {
        description: 'CaseStudy DELETE success',
      },
    },
  })
  @authenticate('jwt')
  @authorize(AUTHORIZE_ADMIN)
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.caseStudyRepository.deleteById(id);
  }
}
