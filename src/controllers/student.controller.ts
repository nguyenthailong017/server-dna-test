import {UserService} from '@loopback/authentication';
import {inject} from '@loopback/core';
import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  del,
  requestBody,
  response,
} from '@loopback/rest';
import {pick} from 'ramda';
import {PasswordHasherBindings, UserServiceBindings} from '../keys';
import {Student, User} from '../models';
import {Credentials, StudentRepository, UserRepository} from '../repositories';
import {
  PasswordHasher,
  UserManagementService,
  validateCredentials,
} from '../services';
import {NewUserRequest} from './user-management.controller';

export class StudentController {
  constructor(
    @repository(StudentRepository)
    public studentRepository: StudentRepository,
    @inject(UserServiceBindings.USER_SERVICE)
    public userService: UserService<User, Credentials>,
    @inject(UserServiceBindings.USER_SERVICE)
    public userManagementService: UserManagementService,
    @repository(UserRepository)
    public userRepository: UserRepository,
    @inject(PasswordHasherBindings.PASSWORD_HASHER)
    public passwordHasher: PasswordHasher,
  ) {}

  @post('/students')
  @response(200, {
    description: 'Student model instance',
    content: {'application/json': {schema: getModelSchemaRef(Student)}},
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: {
            title: 'NewPost',
            type: 'object',
            properties: {
              student: {
                type: 'object',
                properties: {
                  ...getModelSchemaRef(Student, {
                    title: 'NewStudent',
                    exclude: ['id'],
                  }).definitions['NewStudent'].properties,
                },
              },
              user: {
                type: 'object',
                properties: {
                  ...getModelSchemaRef(NewUserRequest, {
                    title: 'NewUser',
                    exclude: ['id'],
                  }).definitions['NewUser'].properties,
                },
              },
            },
          },
        },
      },
    })
    student: {
      user: NewUserRequest;
      student: Student;
    },
  ): Promise<{
    user: User;
    student: Student;
  }> {
    try {
      validateCredentials(pick(['email', 'password'], student.user));
      const resUser = await this.userManagementService.createUser(student.user);
      const resStudent = await this.studentRepository.create({
        ...student.student,
        userId: resUser?.id,
      });

      return {
        user: resUser,
        student: resStudent,
      };
    } catch (e) {
      throw e;
    }
  }

  @patch('/students/{id}')
  @response(204, {
    description: 'Student PATCH success',
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: {
            title: 'NewPost',
            type: 'object',
            properties: {
              student: {
                type: 'object',
                properties: {
                  ...getModelSchemaRef(Student, {
                    title: 'NewStudent',
                    partial: true,
                  }).definitions['NewStudent'].properties,
                },
              },
              user: {
                type: 'object',
                properties: {
                  ...getModelSchemaRef(NewUserRequest, {
                    title: 'NewUser',
                    partial: true,
                  }).definitions['NewUser'].properties,
                },
              },
            },
          },
        },
      },
    })
    student: {
      user: NewUserRequest;
      student: Student;
    },
  ): Promise<{
    user: User;
    student: Student;
  }> {
    try {
      const resStudent = await this.studentRepository.updateById(id, {
        ...student.student,
      });

      const resUser = await this.userRepository.updateById(
        resStudent?.userId,
        student.user,
      );

      return {
        user: resUser,
        student: resStudent,
      };
    } catch (e) {
      throw e;
    }
  }

  @get('/students/count')
  @response(200, {
    description: 'Student model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(@param.where(Student) where?: Where<Student>): Promise<Count> {
    return this.studentRepository.count(where);
  }

  @get('/students')
  @response(200, {
    description: 'Array of Student model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(Student, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.filter(Student) filter?: Filter<Student>,
  ): Promise<Student[]> {
    return this.studentRepository.find(filter);
  }

  @get('/students/{id}')
  @response(200, {
    description: 'Student model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(Student, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(Student, {exclude: 'where'})
    filter?: FilterExcludingWhere<Student>,
  ): Promise<Student> {
    return this.studentRepository.findById(id, filter);
  }

  @del('/students/{id}')
  @response(204, {
    description: 'Student DELETE success',
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    const user = await this.studentRepository.findById(id, {
      fields: ['id', 'userId'],
    });

    await this.studentRepository.deleteById(id);
    await this.userRepository.deleteById(user.userId);
  }
}
