import moment from 'moment';
import {includes} from 'ramda';

// const days: any = {
//   SUN: 0,
//   MON: 1,
//   TUE: 2,
//   WED: 3,
//   THU: 4,
//   FRI: 5,
//   SAT: 6,
// };

export const getDatesBetween2Date = async ({
  schedule, // [{}]
  startDate,
  shift,
}: {
  schedule: any[];
  startDate: string;
  shift: number;
}) => {
  // schedule [{ day: 'Mon', shiftId: 0, roomId: 0}]
  const formatSchedule = schedule.map((s: any) => s.day);

  const start = moment(startDate);

  const totalDays = (shift / schedule.length) * 7;
  let current = start.clone();

  const end = start.add(totalDays, 'days');

  const results = [];

  while (current.isBefore(end)) {
    const currentDay = moment(current).days();

    if (includes(currentDay, formatSchedule)) {
      const scheduleItem = schedule.find(i => i.day === currentDay);

      results.push({
        start: moment(current).utc().toISOString(),
        roomId: scheduleItem?.roomId,
        shiftId: scheduleItem?.shiftId,
      });
    }
    current = current.add(1, 'days');
  }

  return results;
};
