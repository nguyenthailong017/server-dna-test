import {
  belongsTo,
  Entity,
  hasMany,
  model,
  property,
} from '@loopback/repository';
import {ChangeSchedule} from './change-schedule.model';
import {User} from './user.model';
import {Customer} from './customer.model';
import {Classroom} from './classroom.model';

@model()
export class Student extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'date',
  })
  startCourseDate?: string;

  @property({
    type: 'date',
  })
  endCourseDate?: string;

  @property({
    type: 'number',
  })
  fee?: number;

  @property({
    type: 'string',
  })
  feeStatus?: string;

  @property({
    type: 'date',
  })
  startCourseDate1: string;

  @property({
    type: 'date',
  })
  startCourseDate2: string;

  @property({
    type: 'date',
  })
  startCourseDate3: string;

  @property({
    type: 'date',
  })
  endCourseDate1: string;

  @property({
    type: 'date',
  })
  endCourseDate2: string;

  @property({
    type: 'date',
  })
  endCourseDate3: string;

  @property({
    type: 'string',
  })
  orientation1: string;

  @property({
    type: 'string',
  })
  orientation2: string;

  @property({
    type: 'string',
  })
  orientation3: string;

  @property({
    type: 'string',
    description: 'Quá trình học',
  })
  note: string;

  @property({
    type: 'date',
  })
  lockedAt: string | null;

  @property({
    type: 'string',
  })
  lockMessage: string;

  @hasMany(() => ChangeSchedule, {keyTo: 'studentId'})
  changeSchedules: ChangeSchedule[];

  @belongsTo(() => User)
  userId: number;

  @belongsTo(() => Customer)
  customerId: number;

  @belongsTo(() => Classroom)
  classroomId: number;

  constructor(data?: Partial<Student>) {
    super(data);
  }
}

export interface StudentRelations {
  // describe navigational properties here
}

export type StudentWithRelations = Student & StudentRelations;
