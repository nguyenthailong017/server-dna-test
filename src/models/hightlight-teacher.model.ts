import {Entity, hasOne, model, property} from '@loopback/repository';
import {MediaContent} from './media-content.model';

@model()
export class HightlightTeacher extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'string',
    required: true,
  })
  name: string;

  @property({
    type: 'string',
  })
  introduction?: string;

  @property({
    type: 'number',
    default: 1,
  })
  priority: number;

  @property({
    type: 'date',
  })
  createdAt?: string;

  @property({
    type: 'date',
  })
  updatedAt?: string;

  @hasOne(() => MediaContent)
  mediaContent: MediaContent;

  constructor(data?: Partial<HightlightTeacher>) {
    super(data);
  }
}

export interface HightlightTeacherRelations {
  // describe navigational properties here
}

export type HightlightTeacherWithRelations = HightlightTeacher &
  HightlightTeacherRelations;
