import {Entity, model, property, belongsTo} from '@loopback/repository';
import {User} from './user.model';
import {Schedule} from './schedule.model';
import {Classroom} from './classroom.model';

@model()
export class Attendance extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'string',
  })
  checkinTime?: string;

  @property({
    type: 'string',
  })
  checkoutTime?: string;

  @property({
    type: 'string',
  })
  note?: string;

  @belongsTo(() => User)
  userId: number;

  @belongsTo(() => Schedule)
  scheduleId: number;

  @belongsTo(() => Classroom)
  classroomId: number;

  constructor(data?: Partial<Attendance>) {
    super(data);
  }
}

export interface AttendanceRelations {
  // describe navigational properties here
}

export type AttendanceWithRelations = Attendance & AttendanceRelations;
