import {
  Entity,
  model,
  property,
  hasMany,
  belongsTo,
} from '@loopback/repository';
import {NEWS_TYPES} from '../constants';
import {MediaContent} from './media-content.model';
import {User} from './user.model';

@model({
  settings: {
    indexes: {
      uniqueSlug: {
        keys: {
          slug: 1,
        },
        options: {
          unique: true,
        },
      },
      uniqueTitle: {
        keys: {
          title: 1,
        },
        options: {
          unique: true,
        },
      },
    },
  },
})
export class News extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'string',
    required: true,
  })
  title: string;

  @property({
    type: 'string',
    required: true,
  })
  slug: string;

  @property({
    type: 'string',
    required: true,
  })
  description: string;

  @property({
    type: 'string',
    required: true,
  })
  content: string;

  @property({
    type: 'string',
  })
  tags: string;

  @property({
    type: 'string',
    jsonSchema: {
      enum: NEWS_TYPES,
    },
  })
  type: string;

  @property({
    type: 'number',
    default: 1,
  })
  priority: number;

  @property({
    type: 'boolean',
    default: false,
  })
  locked?: boolean;

  @property({
    type: 'date',
  })
  eventDate: string;

  @property({
    type: 'string',
  })
  eventTime: string;

  @property({
    type: 'string',
  })
  eventAddress: string;

  @property({
    type: 'date',
  })
  createdAt: string;

  @property({
    type: 'date',
  })
  updatedAt?: string;

  @hasMany(() => MediaContent)
  mediaContents: MediaContent[];

  @belongsTo(() => User)
  creatorId: number;

  constructor(data?: Partial<News>) {
    super(data);
  }
}

export interface NewsRelations {
  // describe navigational properties here
  creator: User;
}

export type NewsWithRelations = News & NewsRelations;
