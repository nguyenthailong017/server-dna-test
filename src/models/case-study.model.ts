import { User } from '@loopback/authentication-jwt';
import {belongsTo, Entity, hasOne, model, property} from '@loopback/repository';
import { MediaContent } from './media-content.model';

@model({
  settings: {
    indexes: {
      uniqueSlugCaseStudy: {
        keys: {
          slug: 1,
        },
        options: {
          unique: true,
        },
      },
      uniqueTitleCaseStudy: {
        keys: {
          title: 1,
        },
        options: {
          unique: true,
        },
      },
    },
  },
})
export class CaseStudy extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'string',
    required: true,
  })
  slug: string;

  @property({
    type: 'string',
    required: true,
  })
  title: string;

  @property({
    type: 'string',
  })
  description?: string;

  @property({
    type: 'string',
  })
  teacher?: string;

  @property({
    type: 'string',
  })
  studyTime?: string;

  @property({
    type: 'string',
  })
  result?: string;

  @property({
    type: 'string',
  })
  story?: string;

  @property({
    type: 'date',
  })
  createdAt: string;

  @property({
    type: 'date',
  })
  updatedAt?: string;

  @belongsTo(() => User)
  creatorId: number;

  @hasOne(() => MediaContent)
  mediaContent: MediaContent;

  constructor(data?: Partial<CaseStudy>) {
    super(data);
  }
}

export interface CaseStudyRelations {
  // describe navigational properties here
}

export type CaseStudyWithRelations = CaseStudy & CaseStudyRelations;
