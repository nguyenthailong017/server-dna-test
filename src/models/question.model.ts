import {belongsTo, Entity, hasOne, model, property} from '@loopback/repository';
import { Answer } from './answer.model';
import { User } from './user.model';

@model()
export class Question extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id: number;

  @property({
    type: 'string',
    required: true,
  })
  content: string;

  @property({
    type: 'array',
    required: true,
    itemType: 'string'
  })
  answer: string[];

  @property({
    type: 'date',
  })
  createdAt?: string;

  @property({
    type: 'date',
  })
  updatedAt?: string;

  @belongsTo(() => User)
  creatorId: number;

  @hasOne(() => Answer, {keyTo: 'questionId'})
  answerResult: Answer;

  constructor(data?: Partial<Question>) {
    super(data);
  }
}

export interface QuestionRelations {
  // describe navigational properties here
}

export type QuestionWithRelations = Question & QuestionRelations;
