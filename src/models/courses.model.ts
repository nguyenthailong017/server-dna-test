import {
  belongsTo,
  Entity,
  hasMany,
  model,
  property, hasOne} from '@loopback/repository';
import {MediaContent} from './media-content.model';
import {User} from './user.model';

@model({
  settings: {
    indexes: {
      uniqueSlugCourses: {
        keys: {
          slug: 1,
        },
        options: {
          unique: true,
        },
      },
      uniqueTitleCourses: {
        keys: {
          title: 1,
        },
        options: {
          unique: true,
        },
      },
    },
  },
})
export class Courses extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'string',
    required: true,
  })
  title?: string;

  @property({
    type: 'string',
  })
  slug: string;

  @property({
    type: 'string',
    required: true,
  })
  description?: string;

  @property({
    type: 'string',
  })
  entryPoint?: string;

  @property({
    type: 'string',
  })
  outputPoint?: string;

  @property({
    type: 'string',
  })
  time?: string;

  @property({
    type: 'string',
  })
  duration?: string;

  @property({
    type: 'string',
  })
  price?: string;

  @property({
    type: 'string',
  })
  goal?: string;

  @property({
    type: 'number',
    default: 1,
  })
  priority: number;

  @property({
    type: 'string',
  })
  bannerTitle?: string;

  @property({
    type: 'string',
  })
  bannerDescription?: string;

  @property({
    type: 'date',
  })
  createdAt: string;

  @property({
    type: 'date',
  })
  updatedAt?: string;

  @belongsTo(() => User)
  creatorId: number;

  @hasMany(() => MediaContent)
  mediaContents: MediaContent[];

  @hasOne(() => MediaContent, {keyTo: 'courseBannerId'})
  courseBanner: MediaContent;

  constructor(data?: Partial<Courses>) {
    super(data);
  }
}

export interface CoursesRelations {
  // describe navigational properties here
}

export type CoursesWithRelations = Courses & CoursesRelations;
