import {Entity, model, property} from '@loopback/repository';

export const TODO = 'TODO';
export const DOING = 'DOING';
export const DONE = 'DONE';
export const CONTACT_STATUS = [TODO, DOING, DONE];

@model()
export class Contact extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'string',
  })
  fullname: string;

  @property({
    type: 'string',
  })
  phone: string;

  @property({
    type: 'string',
  })
  email: string;

  @property({
    type: 'string',
  })
  target?: string;

  @property({
    type: 'string',
  })
  time?: string;

  @property({
    type: 'string',
  })
  school?: string;

  @property({
    type: 'string',
    jsonSchema: {
      enum: CONTACT_STATUS,
    },
  })
  status?: string;

  @property({
    type: 'date',
  })
  createdAt?: string;

  @property({
    type: 'date',
  })
  updatedAt?: string;

  constructor(data?: Partial<Contact>) {
    super(data);
  }
}

export interface ContactRelations {
  // describe navigational properties here
}

export type ContactWithRelations = Contact & ContactRelations;
