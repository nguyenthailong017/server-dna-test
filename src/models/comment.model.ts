import {Entity, model, property} from '@loopback/repository';

@model()
export class Comment extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'number',
  })
  commentId?: number;

  @property({
    type: 'boolean',
  })
  isParentComment?: boolean;

  @property({
    type: 'string',
    required: true,
  })
  ownerName?: string;

  @property({
    type: 'string',
    required: true,
  })
  ownerEmail?: string;

  @property({
    type: 'string',
    required: true,
  })
  content?: string;

  @property({
    type: 'date',
  })
  createdAt?: string;

  @property({
    type: 'date',
  })
  updatedAt?: string;

  @property({
    type: 'number',
  })
  blogId?: number;

  constructor(data?: Partial<Comment>) {
    super(data);
  }
}

export interface CommentRelations {
  // describe navigational properties here
  subComments: Comment[];
}

export type CommentWithRelations = Comment & CommentRelations;
