import {
  Entity,
  model,
  property,
  hasMany,
  belongsTo,
} from '@loopback/repository';

import {Department} from './department.model';
import {Timekeeping} from './timekeeping.model';
import {User} from './user.model';

@model()
export class Staff extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'number',
  })
  bhxh?: number;

  @property({
    type: 'number',
  })
  salary?: number;

  @property({
    type: 'date',
  })
  lockedAt: string | null;

  @property({
    type: 'string',
  })
  lockMessage: string;

  @belongsTo(() => Department)
  departmentId: number;

  @hasMany(() => Timekeeping, {keyTo: 'staffId'})
  timekeepings: Timekeeping[];

  @belongsTo(() => User)
  userId: number;

  constructor(data?: Partial<Staff>) {
    super(data);
  }
}

export interface StaffRelations {
  // describe navigational properties here
}

export type StaffWithRelations = Staff & StaffRelations;
