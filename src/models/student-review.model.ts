import {Entity, model, property, belongsTo} from '@loopback/repository';
import {User} from './user.model';
import {Classroom} from './classroom.model';

@model()
export class StudentReview extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'string',
  })
  dateReview?: string;

  @property({
    type: 'string',
    description: 'Điểm mạnh (kiến thức, kỹ năng)',
  })
  strong?: string;

  @property({
    type: 'string',
    description: 'Điểm cần cải thiện (kiến thức, kỹ năng)',
  })
  optimize?: string;

  @property({
    type: 'string',
    description: 'Xu hướng học tập',
  })
  trending?: string;

  @property({
    type: 'string',
    description: 'Action plan',
  })
  action?: string;

  @property({
    type: 'string',
  })
  content?: string;

  @belongsTo(() => User)
  userId: number;

  @belongsTo(() => Classroom)
  classroomId: number;

  constructor(data?: Partial<StudentReview>) {
    super(data);
  }
}

export interface StudentReviewRelations {
  // describe navigational properties here
}

export type StudentReviewWithRelations = StudentReview & StudentReviewRelations;
