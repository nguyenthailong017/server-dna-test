import {Entity, model, property} from '@loopback/repository';

export const EXAM_STATUS_TODO = 'TODO';
const EXAM_STATUS_DOING = 'DOING';
const EXAM_STATUS_DONE = 'DONE';
export const EXAM_STATUS = [
  EXAM_STATUS_TODO,
  EXAM_STATUS_DOING,
  EXAM_STATUS_DONE,
];

@model()
export class ExamManagement extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'string',
    required: true,
  })
  name: string;

  @property({
    type: 'string',
    required: true,
  })
  email: string;

  @property({
    type: 'string',
  })
  phone?: string;

  @property({
    type: 'string',
  })
  target?: string;

  @property({
    type: 'string',
    jsonSchema: {
      enum: EXAM_STATUS,
    },
  })
  status?: string;

  @property({
    type: 'string',
    required: true,
  })
  examResult: string;

  @property({
    type: 'date',
  })
  createdAt?: string;

  @property({
    type: 'date',
  })
  updatedAt?: string;

  constructor(data?: Partial<ExamManagement>) {
    super(data);
  }
}

export interface ExamManagementRelations {
  // describe navigational properties here
}

export type ExamManagementWithRelations = ExamManagement &
  ExamManagementRelations;
