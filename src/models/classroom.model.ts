import {Entity, model, property, belongsTo} from '@loopback/repository';
import {Courses} from './courses.model';
@model()
class ClassSchedule extends Entity {
  @property({
    type: 'number',
  })
  shiftId?: number;

  @property({
    type: 'number',
  })
  roomId?: number;

  @property({
    type: 'number',
    jsonSchema: {
      enum: [0, 1, 2, 3, 4, 5, 6],
    },
  })
  day?: number;
}
@model()
class Teacher extends Entity {
  @property({
    type: 'number',
  })
  teacherId?: number;
}
@model()
class Student extends Entity {
  @property({
    type: 'number',
  })
  studentId?: number;
}
@model()
export class Classroom extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'string',
  })
  name?: string;

  @property.array(ClassSchedule, {
    required: true,
  })
  schedule: ClassSchedule[];

  @property({
    type: 'date',
    required: true,
  })
  startDate?: string;

  @property({
    type: 'number',
  })
  tuition?: number;

  @property({
    type: 'string',
  })
  tuitionType?: string;

  @property({
    type: 'number',
    required: true,
  })
  shift?: number;

  @property({
    type: 'string',
  })
  description?: string;

  @property({
    type: 'string',
  })
  level?: string;

  @property({
    type: 'string',
    description: 'Định hướng học tập',
  })
  orientation: string;

  @property({
    type: 'date',
  })
  createdAt: string;

  @property({
    type: 'date',
  })
  updatedAt?: string;

  @property.array(Number)
  teacherIds: number[];

  @property.array(Number)
  studentIds: number[];

  @belongsTo(() => Courses)
  coursesId: number;

  constructor(data?: Partial<Classroom>) {
    super(data);
  }
}

export interface ClassroomRelations {
  // describe navigational properties here
}

export type ClassroomWithRelations = Classroom & ClassroomRelations;
