import {Entity, model, property, belongsTo} from '@loopback/repository';
import {Staff} from './staff.model';

@model()
export class Timekeeping extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'date',
  })
  date?: string;

  @property({
    type: 'string',
  })
  time?: string;

  @property({
    type: 'string',
  })
  note?: string;

  @property({
    type: 'date',
  })
  createdAt: string;

  @property({
    type: 'date',
  })
  updatedAt?: string;

  @belongsTo(() => Staff)
  staffId: number;

  constructor(data?: Partial<Timekeeping>) {
    super(data);
  }
}

export interface TimekeepingRelations {
  // describe navigational properties here
}

export type TimekeepingWithRelations = Timekeeping & TimekeepingRelations;
