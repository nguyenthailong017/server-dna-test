import {Entity, model, property, hasOne, belongsTo} from '@loopback/repository';
import {ChangeSchedule} from './change-schedule.model';
import {Classroom} from './classroom.model';
import {Shift} from './shift.model';
import {Room} from './room.model';

@model()
export class Schedule extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'date',
  })
  start?: string;

  @property({
    type: 'number',
  })
  documentId?: number;

  @property({
    type: 'date',
  })
  createdAt?: string;

  @hasOne(() => ChangeSchedule)
  changeSchedule: ChangeSchedule;

  @belongsTo(() => Classroom)
  classroomId: number;

  @belongsTo(() => Shift)
  shiftId: number;

  @belongsTo(() => Room)
  roomId: number;

  constructor(data?: Partial<Schedule>) {
    super(data);
  }
}

export interface ScheduleRelations {
  // describe navigational properties here
}

export type ScheduleWithRelations = Schedule & ScheduleRelations;
