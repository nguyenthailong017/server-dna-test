import {Entity, model, property, hasMany, hasOne} from '@loopback/repository';
import {MediaContent} from './media-content.model';

@model()
export class Review extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'string',
    required: true,
  })
  title: string;

  @property({
    type: 'string',
    required: true,
  })
  content: string;

  @property({
    type: 'string',
    required: true,
  })
  ownerName: string;

  @property({
    type: 'number',
    default: 1,
  })
  priority: number;

  @property({
    type: 'boolean',
    default: false,
  })
  locked?: boolean;

  @property({
    type: 'date',
  })
  createdAt: string;

  @property({
    type: 'date',
  })
  updatedAt?: string;

  @hasOne(() => MediaContent)
  mediaContent: MediaContent;

  constructor(data?: Partial<Review>) {
    super(data);
  }
}

export interface ReviewRelations {
  // describe navigational properties here
  ownerAvatar: MediaContent;
}

export type ReviewWithRelations = Review & ReviewRelations;
