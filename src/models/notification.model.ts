import {Entity, model, property} from '@loopback/repository';
import {NOTIFICATION_TYPES} from '../constants';

@model()
export class Notification extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'string',
  })
  content?: string;

  @property({
    type: 'string',
    jsonSchema: {
      enum: NOTIFICATION_TYPES,
    },
  })
  type?: string;

  @property({
    type: 'boolean',
    default: false,
  })
  read?: boolean;

  @property({
    type: 'date',
  })
  createdAt?: string;

  @property({
    type: 'date',
  })
  updatedAt?: string;

  @property({
    type: 'boolean',
    default: false,
  })
  pined?: boolean;

  @property({
    type: 'number',
  })
  receiverId?: number;

  constructor(data?: Partial<Notification>) {
    super(data);
  }
}

export interface NotificationRelations {
  // describe navigational properties here
}

export type NotificationWithRelations = Notification & NotificationRelations;
