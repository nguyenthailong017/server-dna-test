import {Entity, model, property, belongsTo} from '@loopback/repository';
import {User} from './user.model';

@model()
export class StaffSchedule extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'date',
  })
  date?: string;

  @property({
    type: 'string',
  })
  time?: string;

  @property({
    type: 'date',
  })
  createdAt?: string;

  @property({
    type: 'date',
  })
  updatedAt?: string;

  @property({
    type: 'string',
  })
  note?: string;

  @belongsTo(() => User)
  userId: number;

  constructor(data?: Partial<StaffSchedule>) {
    super(data);
  }
}

export interface StaffScheduleRelations {
  // describe navigational properties here
}

export type StaffScheduleWithRelations = StaffSchedule & StaffScheduleRelations;
