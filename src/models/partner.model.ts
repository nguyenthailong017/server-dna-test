import {Entity, model, property, hasOne} from '@loopback/repository';
import {MediaContent} from './media-content.model';

@model()
export class Partner extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'string',
  })
  name?: string;

  @property({
    type: 'string',
  })
  introduct?: string;

  @hasOne(() => MediaContent)
  mediaContent: MediaContent;

  constructor(data?: Partial<Partner>) {
    super(data);
  }
}

export interface PartnerRelations {
  // describe navigational properties here
}

export type PartnerWithRelations = Partner & PartnerRelations;
