import {Entity, model, property} from '@loopback/repository';

@model()
export class Shift extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'string',
  })
  name?: string;

  @property({
    type: 'string',
  })
  startTime?: string;

  @property({
    type: 'string',
  })
  endTime?: string;

  constructor(data?: Partial<Shift>) {
    super(data);
  }
}

export interface ShiftRelations {
  // describe navigational properties here
}

export type ShiftWithRelations = Shift & ShiftRelations;
