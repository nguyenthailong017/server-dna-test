import {Entity, model, property} from '@loopback/repository';

@model({
  settings: {
    indexes: {
      uniqueCode: {
        keys: {
          code: 1,
        },
        options: {
          unique: true,
        },
      },
      uniqueName: {
        keys: {
          name: 1,
        },
        options: {
          unique: true,
        },
      },
    },
  },
})
export class Voucher extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'string',
  })
  name?: string;

  @property({
    type: 'string',
  })
  code?: string;

  @property({
    type: 'string',
  })
  type?: string;

  @property({
    type: 'number',
  })
  value?: number;

  @property({
    type: 'string',
  })
  expiryDate?: string;

  @property({
    type: 'number',
  })
  limitCount?: number;

  constructor(data?: Partial<Voucher>) {
    super(data);
  }
}

export interface VoucherRelations {
  // describe navigational properties here
}

export type VoucherWithRelations = Voucher & VoucherRelations;
