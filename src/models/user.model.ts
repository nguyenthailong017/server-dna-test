import {Entity, model, property, hasOne, hasMany} from '@loopback/repository';
import {UserCredentials} from './user-credentials.model';
import {Notification} from './notification.model';
import {ALL_ROLES} from '../constants';

import {Staff} from './staff.model';
import {Teacher} from './teacher.model';

@model({
  settings: {
    indexes: {
      uniqueEmail: {
        keys: {
          email: 1,
        },
        options: {
          unique: true,
        },
      },
    },
  },
})
export class User extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id: number;

  @property({
    type: 'string',
    jsonSchema: {
      enum: ALL_ROLES,
    },
    required: true,
  })
  roles: string;

  @property({
    type: 'string',
    required: true,
  })
  email: string;

  @property({
    type: 'string',
    required: true,
  })
  firstName?: string;

  @property({
    type: 'string',
    required: true,
  })
  lastName?: string;

  @property({
    type: 'string',
    description: 'Dùng cho search user',
  })
  slug: string;

  @property({
    type: 'string',
  })
  gender: string;

  @property({
    type: 'date',
  })
  birthday: string;

  @property({
    type: 'string',
  })
  phone: string;

  @property({
    type: 'string',
  })
  code?: string;

  @property({
    type: 'string',
  })
  address?: string;

  @property({
    type: 'date',
  })
  lockedAt: string | null;

  @property({
    type: 'string',
  })
  lockMessage: string;

  @property({
    type: 'date',
  })
  createdAt: string;

  @property({
    type: 'date',
  })
  updatedAt: string;

  @property({
    type: 'string',
  })
  resetKey?: string;

  @property({
    type: 'number',
  })
  resetCount: number;

  @property({
    type: 'string',
  })
  resetTimestamp: string;

  @property({
    type: 'string',
  })
  resetKeyTimestamp: string;

  @hasOne(() => UserCredentials)
  userCredentials: UserCredentials;

  @hasMany(() => Notification, {keyTo: 'receiverId'})
  notifications: Notification[];

  @hasOne(() => Staff)
  staff: Staff;

  @hasOne(() => Teacher)
  teacher: Teacher;

  constructor(data?: Partial<User>) {
    super(data);
  }
}

export interface UserRelations {
  // describe navigational properties here
}

export type UserWithRelations = User & UserRelations;
