import {Entity, model, property} from '@loopback/repository';
import {Schedule} from './schedule.model';
import {User} from './user.model';

export const CS_STATUS_TODO = 'TODO';
const CS_STATUS_DOING = 'DOING';
const CS_STATUS_DONE = 'DONE';
export const CHANGE_SCHEDULE_STATUS = [
  CS_STATUS_TODO,
  CS_STATUS_DOING,
  CS_STATUS_DONE,
];
@model()
export class ChangeSchedule extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'date',
  })
  newScheduleDate?: string;

  @property({
    type: 'string',
  })
  reason?: string;

  @property({
    type: 'number',
  })
  studentId?: number;

  @property({
    type: 'number',
  })
  userId?: number;

  @property({
    type: 'number',
  })
  scheduleId?: number;

  @property({
    type: 'object',
  })
  owner?: object;

  @property({
    type: 'object',
  })
  schedule?: object;

  @property({
    type: 'string',
    jsonSchema: {
      enum: CHANGE_SCHEDULE_STATUS,
    },
  })
  status?: string;

  @property({
    type: 'date',
  })
  createdAt?: string;

  @property({
    type: 'date',
  })
  updatedAt?: string;

  constructor(data?: Partial<ChangeSchedule>) {
    super(data);
  }
}

export interface ChangeScheduleRelations {
  // describe navigational properties here
  owner?: User;
  schedule?: Schedule;
}

export type ChangeScheduleWithRelations = ChangeSchedule &
  ChangeScheduleRelations;
