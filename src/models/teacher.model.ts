import {Entity, model, property, belongsTo} from '@loopback/repository';
import {User} from './user.model';

@model()
class TeacherCourses extends Entity {
  @property({
    type: 'number',
  })
  courseId?: number;

  @property({
    type: 'string',
  })
  courseName?: string;
}

@model()
export class Teacher extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'string',
  })
  profile?: string;

  @property({
    type: 'number',
  })
  bhxh?: number;

  @property({
    type: 'number',
  })
  salary?: number;

  @property({
    type: 'date',
  })
  lockedAt: string | null;

  @property({
    type: 'string',
  })
  lockMessage: string;

  @property.array(TeacherCourses)
  courses: TeacherCourses[];

  @belongsTo(() => User)
  userId: number;

  constructor(data?: Partial<Teacher>) {
    super(data);
  }
}

export interface TeacherRelations {
  // describe navigational properties here
}

export type TeacherWithRelations = Teacher & TeacherRelations;
