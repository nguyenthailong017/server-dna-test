import {Entity, model, property, belongsTo} from '@loopback/repository';
import {User} from './user.model';

@model()
export class Customer extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id: number;

  @property({
    type: 'string',
    required: true,
  })
  email: string;

  @property({
    type: 'string',
    required: true,
  })
  firstName?: string;

  @property({
    type: 'string',
    required: true,
  })
  lastName?: string;

  @property({
    type: 'string',
    description: 'Dùng cho search user',
  })
  slug: string;

  @property({
    type: 'string',
  })
  gender: string;

  @property({
    type: 'string',
  })
  phone: string;

  @property({
    type: 'string',
  })
  social: string;

  @property({
    type: 'string',
  })
  address: string;

  @property({
    type: 'date',
  })
  birthday?: string | null;

  @property({
    type: 'string',
    description: 'Ngày tốt nghiệp',
  })
  graduate: string;

  @property({
    type: 'string',
  })
  school: string;

  @property({
    type: 'string',
  })
  parentName: string;

  @property({
    type: 'string',
  })
  parentPhone: string;

  @property({
    type: 'string',
    description: 'Trình độ hiện tại',
  })
  level: string;

  @property({
    type: 'string',
    description: 'Mục tiêu học',
  })
  target: string;

  @property({
    type: 'string',
    description: 'Quốc gia du học - định cư',
  })
  targetCountry: string;

  @property({
    type: 'string',
    description: 'Định hướng học tập',
  })
  orientation: string;

  @property({
    type: 'number',
    description: 'Giáo viên test đầu vào',
  })
  teacherId: number;

  @property({
    type: 'date',
    description: 'Lịch Test dự kiến',
  })
  planInputTest?: string | null;

  @property({
    type: 'date',
    description: 'Lịch thi dự kiến',
  })
  planExam: string | null;

  @property({
    type: 'number',
    description: 'Chương trình học',
  })
  programId: number;

  @property({
    type: 'number',
  })
  userId: number;

  @property({
    type: 'string',
    description: 'Online, PSE, bạn bè, học viên,vãng lai',
  })
  source: string;

  @property({
    type: 'string',
    description: 'Khách hàng thô, Đang tư vấn, Đã tư vấn...',
    default: 'TODO',
  })
  status: string;

  @property({
    type: 'string',
    description: 'Trạng thái chuyển đổi',
  })
  transferStatus: string;

  @property({
    type: 'string',
    description: 'Kết quả test đầu vào',
  })
  inputTestResult: string;

  @property({
    type: 'string',
  })
  note: string;

  @property({
    type: 'string',
    description: 'Lịch sử chăm sóc',
  })
  history: string;

  @property({
    type: 'number',
    description: 'Học phí',
  })
  fee: number;

  @property({
    type: 'string',
    description:
      'Loại học phí ( Nộp học thử, nộp học theo tháng, nộp học theo khóa',
  })
  feeStatus: string;

  @property({
    type: 'date',
  })
  lockedAt: string | null;

  @property({
    type: 'string',
  })
  lockMessage: string;

  @property({
    type: 'date',
  })
  createdAt: string;

  @property({
    type: 'date',
  })
  updatedAt: string;

  @property({
    type: 'number',
  })
  classId: string;

  @belongsTo(() => User)
  staffId: number;

  constructor(data?: Partial<Customer>) {
    super(data);
  }
}

export interface CustomerRelations {
  // describe navigational properties here
}

export type CustomerWithRelations = Customer & CustomerRelations;
