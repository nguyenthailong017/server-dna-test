import {Entity, model, property, belongsTo} from '@loopback/repository';
import {Review, News} from '.';
import {MEDIA_SOURCE_TYPES, MEDIA_TYPES, MEDIA_TYPES_PHOTO} from '../constants';
@model()
export class MediaContent extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'string',
  })
  filePath: string;

  @property({
    type: 'string',
    required: true,
    jsonSchema: {
      enum: MEDIA_TYPES,
    },
    default: MEDIA_TYPES_PHOTO,
  })
  mediaType: string;

  @property({
    type: 'string',
  })
  key?: string;

  @property({
    type: 'string',
  })
  blurKey?: string;

  @property({
    type: 'string',
  })
  videoKey?: string;

  @property({
    type: 'string',
    required: true,
  })
  url: string;

  @property({
    type: 'string',
  })
  urlBlur?: string;

  @property({
    type: 'string',
  })
  videoUrl: string;

  @property({
    type: 'string',
    jsonSchema: {
      enum: MEDIA_SOURCE_TYPES,
    },
  })
  sourceType: string;

  @property({
    type: 'number',
    default: 1,
  })
  priority: number;

  @property({
    type: 'number',
  })
  size: number;

  @property({
    type: 'number',
  })
  width: number;

  @property({
    type: 'number',
  })
  height: number;

  @property({
    type: 'string',
  })
  dominantColor: string;

  @property({
    type: 'string',
  })
  palette: string;

  @property({
    type: 'date',
  })
  createdAt: string;

  @belongsTo(() => News)
  newsId: number;

  @belongsTo(() => Review)
  reviewId: number;

  @property({
    type: 'number',
  })
  photoId?: number;

  @property({
    type: 'number',
  })
  partnerId?: number;

  @property({
    type: 'number',
  })
  hightlightTeacherId?: number;

  @property({
    type: 'number',
  })
  caseStudyId?: number;

  @property({
    type: 'number',
  })
  coursesId?: number;

  @property({
    type: 'number',
  })
  courseBannerId?: number;

  constructor(data?: Partial<MediaContent>) {
    super(data);
  }
}

export interface MediaContentRelations {
  // describe navigational properties here
}

export type MediaContentWithRelations = MediaContent & MediaContentRelations;
