import {Entity, model, property} from '@loopback/repository';

@model()
export class Document extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'string',
  })
  name?: string;

  @property({
    type: 'string',
  })
  link?: string;

  @property({
    type: 'string',
  })
  type?: string;

  @property({
    type: 'date',
  })
  createdAt?: string;


  constructor(data?: Partial<Document>) {
    super(data);
  }
}

export interface DocumentRelations {
  // describe navigational properties here
}

export type DocumentWithRelations = Document & DocumentRelations;
