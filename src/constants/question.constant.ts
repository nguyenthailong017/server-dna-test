import { Question } from "../models";

export interface QuestionRequestInterface extends Partial<Question> {
    correctAnswer?: string;
  }