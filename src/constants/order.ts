export const ORDER = {
  PRIORITY_DESC: ['priority DESC', 'createdAt DESC'],
  LATEST: ['createdAt DESC'],
  OLDEST: ['createdAt ASC'],
};
