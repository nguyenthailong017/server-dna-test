import {basicAuthorization} from '../services/basic.authorizor';

export const USER_ROLE_ADMIN = 'admin';
export const USER_ROLE_STAFF = 'staff';
export const USER_ROLE_TEACHER = 'teacher';
export const USER_ROLE_STUDENT = 'student';
export const USER_ROLE_CUSTOMER = 'customer';

const ADMIN_ROLES = [USER_ROLE_ADMIN];
const STAFF_ROLES = [USER_ROLE_STAFF, USER_ROLE_ADMIN];
const TEACHER_ROLES = [USER_ROLE_TEACHER, USER_ROLE_ADMIN];
const STUDENT_ROLES = [USER_ROLE_STUDENT, USER_ROLE_ADMIN];
const CUSTOMER_ROLES = [USER_ROLE_CUSTOMER, USER_ROLE_ADMIN];

const STAFF_TEACHER_ROLES = [
  USER_ROLE_STAFF,
  USER_ROLE_TEACHER,
  USER_ROLE_ADMIN,
];

export const ALL_ROLES = [
  USER_ROLE_STAFF,
  USER_ROLE_ADMIN,
  USER_ROLE_TEACHER,
  USER_ROLE_STUDENT,
];

export const AUTHORIZE_ADMIN = {
  allowedRoles: ADMIN_ROLES,
  voters: [basicAuthorization],
};

export const AUTHORIZE_MODERATOR = {
  allowedRoles: STAFF_TEACHER_ROLES,
  voters: [basicAuthorization],
};

export const AUTHORIZE_CUSTOMER = {
  allowedRoles: CUSTOMER_ROLES,
  voters: [basicAuthorization],
};

export const AUTHORIZE_ALL = {
  allowedRoles: ALL_ROLES,
  voters: [basicAuthorization],
};
