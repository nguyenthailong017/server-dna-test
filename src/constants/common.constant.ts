export const NEWS = 'NEWS';
export const EVENT = 'EVENT';
export const NEWS_TYPES = [EVENT, NEWS];

export const NOTIFICATION_TYPES = ['FROM_ADMIN', 'HOMEWORK', 'DOCUMENT'];
