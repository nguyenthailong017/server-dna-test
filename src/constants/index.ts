export * from './media-content.constant';
export * from './auth.constant';
export * from './common.constant';

export const TRANSACTION_TIMEOUT = process.env.TRANSACTION_TIMEOUT
  ? Number(process.env.TRANSACTION_TIMEOUT)
  : 600000; // 10 mins in miliseconds
